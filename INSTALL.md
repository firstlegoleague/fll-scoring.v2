# Installing FLL Tools

The process is very similar for production and development

## Get the dependencies
For the application self you need the following packages
`php7.4 composer php-mbstring php-mysql php-zip php-simplexml php-gd php-fpm`

For the database FLL scoring utilizes `mariadb-server`

Some handy packages are
`curl nano`

## Install nodejs
Follow the guide / installers from [nodeJS](https://nodejs.org/en/download/)

## Install the database
Digital ocean has some really good tutorials on how to set it up, such as [these](https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04)
And create a user and database. Those credentials needs to be saved for later.

## Clone the repository 
For production, somewhere in /var/www/, if you are doing development, you can clone it where ever you want.
I don't hope there needs to be a guide on how to clone a git repository

## Set up the env file
Copy the `.env.example` and rename it it to `.env`. Open this file in your favourite editor and change the settings.
One very important one to change is the `ADMIN_PASSWORD`. This is de default password for the admin account (admin@fllscoring.nl)
No matter if you run in production or development, change this value.

## Install other dependencies
To get the others dependencies, we use npm and composer.
First install `composer install` inside of the root folder of the repository, once that finishes, 
you can run `npm i; npm run dev` or `npm i; npm run prod` depending on the usecase.

## Setup database
To setup the database, you need to run the following commands:
 - `php artisan migrate`
 - `php artisan db:seed`

## Prod only

If you want to run it in production, you should be able do this yourself. If you need help, you can visit us via
[#fllscoring on libera.chat](https://web.libera.chat/#fllscoring)

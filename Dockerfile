FROM existenz/webstack:8.0

COPY --chown=php:nginx ./ /www
COPY --chown=php:nginx ./.env.example /www/.env

RUN find /www -type d -exec chmod -R 555 {} \; \
    && find /www -type f -exec chmod -R 444 {} \; \
    && apk -U --no-cache add \
    git \
    nodejs \
    npm \
    php8 \
    php8-ctype \
    php8-dom \
    php8-fileinfo \
    php8-gd \
    php8-iconv \
    php8-json \
    php8-mbstring \
    php8-openssl \
    php8-pdo \
    php8-pdo_mysql \
    php8-phar \
    php8-session \
    php8-simplexml \
    php8-tokenizer \
    bind-tools \
    php8-xml \
    php8-xmlreader \
    php8-xmlwriter \
    php8-zip \
    php8-curl

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

WORKDIR /www

RUN chmod 777 /www/storage/ -R

RUN ln -s /usr/bin/php8 /usr/bin/php
RUN ["/usr/bin/composer", "config", "--global", "disable-tls", "true"]
RUN ["/usr/bin/composer", "install"]

RUN chown php:nginx -R /www/

HEALTHCHECK CMD curl --fail http://localhost:80 || exit 1

# Frontend
RUN npm install
RUN npm run production

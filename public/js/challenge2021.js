// Challenge M00 | Inspection
function js_M00() {
    var score = 0;
    var element = document.getElementById("M00");
    var M00 = document.getElementsByName("M00_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M00[1].checked) {
        score = 20;
    }

    //Check if it valid
    for (var i = 0, length = M00.length; i < length; i++) {
        if (M00[i].checked) {
            check = check + 1;
            element.classList.add("alert-success");
            break;
        }
    }

    document.getElementById("M00_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;
}

// Challenge M01 | Innovation Project
function js_M01() {
    var score = 0;
    var element = document.getElementById("M01");
    var M01 = document.getElementsByName("M01_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M01[1].checked) {
        score = 20;
    }

    //Check if it valid
    for (var i = 0, length = M01.length; i < length; i++) {
        if (M01[i].checked) {
            check = check + 1;
            element.classList.add("alert-success");
            break;
        }
    }

    document.getElementById("M01_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M02() {
    var score = 0;
    var element = document.getElementById("M02");
    var M02_1 = document.getElementsByName("M02_1");
    var M02_2 = document.getElementsByName("M02_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M02_2[0].checked){
        score = 0;
    } else if(M02_2[1].checked){
        score = 20;
    } else if(M02_2[2].checked){
        score = 30;
    }

    if(M02_1[0].checked){
        score = 0;
    }
    document.getElementById("M02_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M02_1.length; i < length; i++) {
        if (M02_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M02_2.length; i < length; i++) {
        if (M02_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M03() {
    var score = 0;
    var element = document.getElementById("M03");
    var M03_1 = document.getElementsByName("M03_1");
    var M03_2 = document.getElementsByName("M03_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M03_1[1].checked){
        score = score + 20;
    }

    if(M03_2[1].checked){
        score = score + 10;
    }
    document.getElementById("M03_pt").innerText = score + " pt";


    //Check if it valid
    for (var i = 0, length = M03_1.length; i < length; i++) {
        if (M03_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M03_2.length; i < length; i++) {
        if (M03_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M04() {
    var score = 0;
    var element = document.getElementById("M04");
    var M04_1 = document.getElementsByName("M04_1");
    var M04_2 = document.getElementsByName("M04_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M04_2[1].checked || M04_1[1].checked){
        score = 10;
    }

    if(M04_2[1].checked && M04_1[1].checked){
        score = 30;
    }


    document.getElementById("M04_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M04_1.length; i < length; i++) {
        if (M04_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M04_2.length; i < length; i++) {
        if (M04_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M05() {
    var score = 0;
    var element = document.getElementById("M05");
    var M05_1 = document.getElementsByName("M05_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M05_1[1].checked){
        score = 20;
    }
    document.getElementById("M05_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M05_1.length; i < length; i++) {
        if (M05_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M06() {
    var score = 0;
    var element = document.getElementById("M06");
    var M06_1 = document.getElementsByName("M06_1");
    var M06_2 = document.getElementsByName("M06_2");
    var M06_3 = document.getElementsByName("M06_3");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M06_1[1].checked){
        score = 20;
    }

    if(M06_1[1].checked && M06_2[1].checked){
        score = 30;
    }

    if(M06_3[1].checked){
        score = 0;
    }

    // Mission is unclear
    // TODO implement logic here

    document.getElementById("M06_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M06_1.length; i < length; i++) {
        if (M06_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M06_2.length; i < length; i++) {
        if (M06_2[i].checked) {
            check = check + 1;
            break;
        }
    }
    for (var i = 0, length = M06_3.length; i < length; i++) {
        if (M06_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3){
        element.classList.add("alert-success");
        // element.classList.add("alert-danger");
        // To remind myself something is wrong
        // TODO change when this issue is fixed
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M07() {
    var score = 0;
    var element = document.getElementById("M07");
    var M07_1 = document.getElementsByName("M07_1");
    var M07_2 = document.getElementsByName("M07_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var error = document.getElementById("M07-error");
    error.classList.add("hideError");

    var check = 0;

    if(M07_1[1].checked){
        score = score + 20;
    }
    if(M07_2[1].checked){
        score = score + 10;
    }

    document.getElementById("M07_pt").innerText = score + " pt";

    if(M07_2[1].checked && M07_1[0].checked){
        score = -1;
        document.getElementById("M07_pt").innerText = "0 pt";
    }

    //Check if it valid
    for (var i = 0, length = M07_1.length; i < length; i++) {
        if (M07_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M07_2.length; i < length; i++) {
        if (M07_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2 && score != -1){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    if(score == -1){
        document.getElementById("M07_pt").innerText = "0 pt";
        element.classList.add("alert-danger");
        if(check == 2)
            error.classList.remove("hideError");
    } else {
        document.getElementById("M07_pt").innerText = score + " pt";
    }

    return score;
}

function js_M08() {
    var score = 0;
    var element = document.getElementById("M08");
    var M08_1 = document.getElementsByName("M08_1");
    var M08_2 = document.getElementsByName("M08_2");
    var M08_3 = document.getElementsByName("M08_3");
    var error = document.getElementById("M08-error");

    error.classList.add("hideError");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M08_1[1].checked){
        score = score + 20;
    }

    if(M08_2[1].checked){
        score = score + 10;
    }

    if(M08_3[1].checked){
        score = score + 10;
    }
    document.getElementById("M08_pt").innerText = score + " pt";

    // if(M08_3[1].checked){
    //     if(!(M08_1[1].checked && M08_2[1].checked)){
    //         score = -1;
    //     }
    // }

    //Check if it valid
    for (var i = 0, length = M08_1.length; i < length; i++) {
        if (M08_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M08_2.length; i < length; i++) {
        if (M08_2[i].checked) {
            check = check + 1;
            break;
        }
    }
    for (var i = 0, length = M08_3.length; i < length; i++) {
        if (M08_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    if(score == -1){
        document.getElementById("M08_pt").innerText = "0 pt";
        element.classList.add("alert-danger");
        if(check == 3)
            error.classList.remove("hideError");
    } else {
        document.getElementById("M08_pt").innerText = score + " pt";
    }

    return score;
}

function js_M09() {
    var score = 0;
    var element = document.getElementById("M09");
    var M09_1 = document.getElementsByName("M09_1");
    var M09_2 = document.getElementsByName("M09_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M09_1[1].checked){
        score = score + 20;
    }

    if(M09_2[1].checked){
        score = score + 20;
    }

    document.getElementById("M09_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M09_1.length; i < length; i++) {
        if (M09_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M09_2.length; i < length; i++) {
        if (M09_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M10() {
    var score = 0;
    var element = document.getElementById("M10");
    var M10_1 = document.getElementsByName("M10_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M10_1[1].checked){
        score = 20;
    }
    document.getElementById("M10_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M10_1.length; i < length; i++) {
        if (M10_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M11() {
    var score = 0;
    var element = document.getElementById("M11");
    var M11_1 = document.getElementsByName("M11_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M11_1[0].checked){
        score = 0;
    } else if(M11_1[1].checked){
        score = 20;
    } else if(M11_1[2].checked){
        score = 30;
    }

    document.getElementById("M11_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M11_1.length; i < length; i++) {
        if (M11_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M12() {
    var score = 0;
    var element = document.getElementById("M12");
    var M12_1 = document.getElementsByName("M12_1");
    var M12_2 = document.getElementsByName("M12_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M12_1[0].checked){
        score = 0;
    } else if(M12_1[1].checked){
        score = 20;
    } else if(M12_1[2].checked){
        score = 30;
    }

    if(M12_2[0].checked){
        score = score + 0;
    } else if(M12_2[1].checked){
        score = score + 5;
    } else if(M12_2[2].checked){
        score = score + 10;
    }
    document.getElementById("M12_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M12_1.length; i < length; i++) {
        if (M12_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M12_2.length; i < length; i++) {
        if (M12_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M13() {
    var score = 0;
    var element = document.getElementById("M13");
    var M13_1 = document.getElementsByName("M13_1");
    var M13_2 = document.getElementsByName("M13_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if (M13_1[1].checked && M13_2[1].checked){
        score = 30;
    } else if(M13_1[1].checked || M13_2[1].checked){
        score = 10;
    }

    document.getElementById("M13_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M13_1.length; i < length; i++) {
        if (M13_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M13_2.length; i < length; i++) {
        if (M13_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 2){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M14() {
    var score = 0;
    var element = document.getElementById("M14");
    var M14_1 = document.getElementsByName("M14_1");
    var M14_2 = document.getElementsByName("M14_2");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;

    if(M14_1[1].checked){
        score = 10;
    } else if(M14_1[2].checked){
        score = 20;
    }
    document.getElementById("M14_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M14_1.length; i < length; i++) {
        if (M14_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M15() {
    var score = 0;
    var element = document.getElementById("M15");
    var M15_1 = document.getElementsByName("M15_1");
    var M15_2 = document.getElementsByName("M15_2");
    var M15_3 = document.getElementsByName("M15_3");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;


    if(M15_1[0].checked){
        score = score + 0;
    } else if(M15_1[1].checked){
        score = score + 10;
    } else if(M15_1[2].checked){
        score = score + 20;
    }

    if(M15_2[0].checked){
        score = score + 0;
    } else if(M15_2[1].checked){
        score = score + 20;
    } else if(M15_2[2].checked){
        score = score + 40;
    }

    if(M15_3[0].checked){
        score = score + 0;
    } else if(M15_3[1].checked){
        score = score + 30;
    } else if(M15_3[2].checked){
        score = score + 60;
    }

    document.getElementById("M15_pt").innerText = score + " pt";

    //Check if it valid
    for (var i = 0, length = M15_1.length; i < length; i++) {
        if (M15_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    //Check if it valid
    for (var i = 0, length = M15_2.length; i < length; i++) {
        if (M15_2[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M15_3.length; i < length; i++) {
        if (M15_3[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 3){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    return score;
}

function js_M16() {
    var score = 0;
    var element = document.getElementById("M16");
    var M16_1 = document.getElementsByName("M16_1");
    var M16_2 = document.getElementsByName("M16_2");
    var M16_3 = document.getElementsByName("M16_3");
    var M16_4 = document.getElementsByName("M16_4");
    var M16_5 = document.getElementsByName("M16_5");

    var M03_2 = document.getElementsByName("M03_2");
    var M10_1 = document.getElementsByName("M10_1");

    var M15_1 = document.getElementsByName("M15_1");
    var M15_2 = document.getElementsByName("M15_2");
    var M15_3 = document.getElementsByName("M15_3");

    var error = document.getElementById("M16-error");
    error.classList.add("hideError");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var check = 0;
    var containers = 0;

    // Container that is stored in the airplane
    if(M03_2[0].checked){
        containers = containers + 1;
    }

    // If the orange on is still in place.
    if(M10_1[1].checked){
        containers = containers + 1;
    }

    // If containers on the trucks count no in the mission, uncommand
    // if(M15_1[1].checked){
    //     containers = containers + 1;
    // }
    // if(M15_1[2].checked){
    //     containers = containers + 2;
    // }

    // On the train
    if(M15_2[1].checked){
        containers = containers + 1;
    }
    if(M15_2[2].checked){
        containers = containers + 2;
    }

    // On the boat
    if(M15_3[1].checked){
        containers = containers + 1;
    }
    if(M15_3[2].checked){
        containers = containers + 2;
    }


    for (var i = 0, length = M16_1.length; i < length; i++) {
        if (M16_1[i].checked) {
            score = score + (i*5)
            containers = containers + i;
            break;
        }
    }

    for (var i = 0, length = M16_2.length; i < length; i++) {
        if (M16_2[i].checked) {
            score = score + (i*10)
            containers = containers + i;
            break;
        }
    }

    if(M16_3[1].checked){
        score = score + 20;
    }

    if(M16_4[1].checked){
        score = score + 20;
    }

    for (var i = 0, length = M16_5.length; i < length; i++) {
        if (M16_5[i].checked) {
            score = score + (i*10)
            break;
        }
    }



    //Check if it valid
    for (var i = 0, length = M16_1.length; i < length; i++) {
        if (M16_1[i].checked) {
            check = check + 1;
            break;
        }
    }
    for (var i = 0, length = M16_2.length; i < length; i++) {
        if (M16_2[i].checked) {
            check = check + 1;
            break;
        }
    }
    for (var i = 0, length = M16_3.length; i < length; i++) {
        if (M16_3[i].checked) {
            check = check + 1;
            break;
        }
    }
    for (var i = 0, length = M16_4.length; i < length; i++) {
        if (M16_4[i].checked) {
            check = check + 1;
            break;
        }
    }

    for (var i = 0, length = M16_5.length; i < length; i++) {
        if (M16_5[i].checked) {
            check = check + 1;
            break;
        }
    }

    // Do the check for the containers
    if(containers > 8){
        element.classList.add("alert-danger");
        error.classList.remove("hideError");
        // The score will be invalid
        score = -1;
    }

    if(check == 5){
        element.classList.add("alert-success");
    } else {
        score = -1
        element.classList.add("alert-danger");
    }

    if(score == -1){
        document.getElementById("M16_pt").innerText = "0 pt";
    } else {
        document.getElementById("M16_pt").innerText = score + " pt";
    }

    return score;
}

function js_M17() {

    var score = 0;
    var element = document.getElementById("M17");

    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");


    var M17_1 = document.getElementsByName("M17_1");
    var check = 0;

    // Validity check
    for (var i = 0, length = M17_1.length; i < length; i++) {
        if (M17_1[i].checked) {
            check = check + 1;
            break;
        }
    }

    if(check == 1) {
        element.classList.remove("alert-danger");
        element.classList.add("alert-success");

        // Score check | Only if field is valid
        if(M17_1[1].checked){
            score = 10
        } else if(M17_1[2].checked){
            score = 15
        }else if(M17_1[3].checked){
            score = 25
        }else if(M17_1[4].checked){
            score = 35
        }else if(M17_1[5].checked){
            score = 50
        }else if(M17_1[6].checked){
            score = 50
        }
    }
    document.getElementById("M17_pt").innerText = score + " pt";

    if(check != 1) {
        score = -1;
        element.classList.add("alert-danger");
    }
    return score;

}

function js_GP(){
    var element = document.getElementById("GP");
    var GP_1 = document.getElementsByName("GP_1");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");

    var check = 0

    for (var i = 0, length = GP_1.length; i < length; i++) {
        if (GP_1[i].checked) {
            check = 1;
            break;
        }
    }

    if(check == 1){
        element.classList.add("alert-success");
    } else {
        check = -1
        element.classList.add("alert-danger");
    }

    return check
}

function calcScore(){

    var element = document.getElementById("score");
    element.classList.remove("alert-success");
    element.classList.remove("alert-danger");
    var score = 0;
    var fail = 0;

    js_M00();
    if (js_M00() != -1){
        score = score + Number(js_M00())
    }else {
        console.log("M00");
        fail = 1
    }

    js_M01();
    if (js_M01() != -1){
        score = score + Number(js_M01())
    }else {
        console.log("M01");
        fail = 1
    }

    js_M02();
    if (js_M02() != -1){
        score = score + Number(js_M02())
    }else {
        console.log("M02");
        fail = 1
    }

    js_M03();
    if (js_M03() != -1){
        score = score + Number(js_M03())
    }else {
        console.log("M03");
        fail = 1
    }

    js_M04();
    if (js_M04() != -1){
        score = score + Number(js_M04())
    }else {
        console.log("M04");
        fail = 1
    }

    js_M05();
    if (js_M05() != -1){
        score = score + Number(js_M05())
    }else {
        console.log("M05");
        fail = 1
    }

    js_M06();
    if (js_M06() != -1){
        score = score + Number(js_M06())
    }else {
        console.log("M06");
        fail = 1
    }

    js_M07();
    if (js_M07() != -1){
        score = score + Number(js_M07())
    }else {
        console.log("M07");
        fail = 1
    }

    js_M08();
    if (js_M08() != -1){
        score = score + Number(js_M08())
    }else {
        console.log("M08");
        fail = 1
    }

    js_M09();
    if (js_M09() != -1){
        score = score + Number(js_M09())
    }else {
        console.log("M09");
        fail = 1
    }

    js_M10();
    if (js_M10() != -1){
        score = score + Number(js_M10())
    }else {
        console.log("M10");
        fail = 1
    }

    js_M11();
    if (js_M11() != -1){
        score = score + Number(js_M11())
    }else {
        console.log("M11");
        fail = 1
    }

    js_M12();
    if (js_M12() != -1){
        score = score + Number(js_M12())
    }else {
        console.log("M12");
        fail = 1
    }

    js_M13();
    if (js_M13() != -1){
        score = score + Number(js_M13())
    }else {
        console.log("M13");
        fail = 1
    }

    js_M14();
    if (js_M14() != -1){
        score = score + Number(js_M14())
    }else {
        console.log("M14");
        fail = 1
    }

    js_M15();
    if (js_M15() != -1){
        score = score + Number(js_M15())
    } else {
        console.log("M15");
        fail = 1
    }

    js_M16();
    if (js_M16() != -1){
        score = score + Number(js_M16())
    } else {
        console.log("M16");
        fail = 1
    }

    js_M17();
    if (js_M17() != -1){
        score = score + Number(js_M17())
    } else {
        console.log("M17");
        fail = 1
    }

    js_GP();
    if(js_GP() == -1){
        console.log("GP");
        fail = 1
    }

    console.log("Score: " + score);
    console.log("Fail: " + fail);

    if(fail == 1){
        element.classList.add("alert-danger");
        return -1
    } else {
        element.classList.add("alert-success");
        document.getElementById("total_pt").innerText =  "  " + score + " pt";
        document.getElementById("total_pt_form").setAttribute("value", score);
        var btn = document.getElementById("submitButton");

        btn.disabled = false;
        btn.classList.remove("button_disabled")

        return score
    }

}

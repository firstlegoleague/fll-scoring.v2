var isRunning = false;

function playAudio(audio){
    return new Promise(res=>{
      audio.play()
      audio.onended = res
    })
  }

async function timer_start() {

    if(isRunning == true){
        return;
    }

    isRunning = true;

    var audio = new Audio('../sounds/start.mp3');
    await playAudio(audio);

    timer = setInterval(function () {
        //
        var elementMin = document.getElementById("timer_min");
        var elementSec = document.getElementById("timer_sec");

        // Parse to interget
        var min = parseInt(elementMin.innerText);
        var sec = parseInt(elementSec.innerText);

        // Do the minute calculation
        if (min == 0 && sec == 0) {
            clearInterval(timer)
        }
        else if (sec == 0) {
            sec = 60
            min--;
        }

        // Remove a second
        sec--;


        // Prevent negative time.
        if (sec == -1) {
            sec = 0
            // Play sound
            var audio = new Audio('../sounds/end.mp3');
            audio.play();
            clearInterval(timer);
        }

        if (min == 0 && sec == 30) {
            var audio = new Audio('../sounds/end-game.mp3');
            audio.play();
        }

        // Render the second
        if (sec < 10) {
            elementSec.innerText = '0' + sec;
        } else {
            elementSec.innerText = sec;
        }

        // Render the minute
        elementMin.innerText = min;

    }, 1000);
}

function timer_stop() {
    var audio = new Audio('../sounds/stop.mp3');
    audio.play();
    clearInterval(timer);
    isRunning = false;
}

function timer_reset() {
    clearInterval(timer);
    isRunning = false;
    var elementMin = document.getElementById("timer_min");
    var elementSec = document.getElementById("timer_sec")
    elementMin.innerText = "2";
    elementSec.innerText = "30";

}

function timer_test_end() {
    var audio = new Audio('../sounds/end.mp3');
    audio.play();
}

function timer_test_stop() {
    var audio = new Audio('../sounds/stop.mp3');
    audio.play();
}

function timer_test_start() {
    var audio = new Audio('../sounds/start.mp3');
    audio.play();
}

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}


function getAnchor() {
    var currentUrl = document.URL,
        urlParts = currentUrl.split('#');

    return (urlParts.length > 1) ? urlParts[1] : null;

}

const client = mqtt.connect('wss://mqtt.fllscoring.nl:9001') // you add a ws:// url here
client.subscribe("fll/timer/" + getAnchor() + "/start")
client.subscribe("fll/timer/" + getAnchor() + "/stop")
client.subscribe("fll/timer/" + getAnchor() + "/reset")

client.on("message", function (topic, payload) {
    var action = topic.substring(topic.lastIndexOf('/') + 1);
    console.log(action)
    if (action == 'start') {
        timer_start()
    } else if (action == 'stop') {
        timer_stop()
    } else if (action == 'reset') {
        timer_reset()
    }
    // client.end()
})

client.publish("fll/log/" + getAnchor(), "hello world!")
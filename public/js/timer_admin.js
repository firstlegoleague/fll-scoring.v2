

const client = mqtt.connect('wss://mqtt.fllscoring.nl:9001') // you add a ws:// url here

function send_start(name){
    client.publish("fll/timer/" + name + "/start", "start-admin")
}

function send_stop(name){
    client.publish("fll/timer/" + name + "/stop", "stop-admin")
}

function send_reset(name){
    client.publish("fll/timer/" + name + "/reset", "reset-admin")
}
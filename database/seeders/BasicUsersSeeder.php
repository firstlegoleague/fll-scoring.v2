<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Foundation\Auth\User;

class BasicUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                // Create an Administrator account
                $user = new \App\Models\User();
                $user->name = "Administrator";
                $user->email = "admin@fllscoring.nl";
                $user->password = Hash::make(env("ADMIN_PASSWORD", "password"));
                $user->save();
        
                // Give admin the super admin rights
                $role = Role::findByName('super-admin');
                $role->users()->attach($user);
        
        
                // Operational Partner
                $nfo = new \App\Models\User();
                $nfo->name = env("OP_NAME", "Operational Partner");
                $nfo->email = env("OP_EMAIL", "op@fllscoring.nl");
                $nfo->password = Hash::make(env('OP_PASSWORD', "password"));
                $nfo->save();
        
                $role_nfo = Role::findByName("nationalorganization");
                $role_nfo->users()->attach($nfo);
        
                // Regional Partner
                $local = new \App\Models\User();
                $local->name = "Organisatie";
                $local->email = "organisatie@fllscoring.nl";
                $local->password = Hash::make("password");
                $local->save();
        
                $local_role = Role::findByName("localorganization");
                $local_role->users()->attach($local);

                // Generic Referee
                $ref = new \App\Models\User();
                $ref->name = "Scheidsrechter";
                $ref->email = "ref@fllscoring.nl";
                $ref->password = Hash::make("wachtwoord");
                $ref->save();
        
                $role_ref = Role::findByName("referee");
                $role_ref->users()->attach($ref);
    }
}

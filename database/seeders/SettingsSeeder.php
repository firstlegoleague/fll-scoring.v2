<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => "finalName",
            'value' => "NotSet",
        ]);

        DB::table('settings')->insert([
            'key' => "season",
            'value' => "2022",
        ]);

        DB::table('settings')->insert([
            'key' => "register",
            'value' => "1",
        ]);

        DB::table('settings')->insert([
            'key' => "gamePublic",
            'value' => "0",
        ]);
    }
}

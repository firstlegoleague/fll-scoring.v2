<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('overview.gameTitle')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card-body">

                        <table data-toggle="table" data-search="true">
                            <thead>
                                <tr>
                                    <th data-sortable="true">{{__('teams.number')}}</th>
                                    <th data-sortable="true">{{__('teams.name')}}</th>
                                    @foreach($rounds as $round)
                                    <th data-sortable="true">{{$round->round}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($teams as $team)
                                <tr>
                                    <td>{{$team->teamNumber}}</td>
                                    <td>{{$team->teamname}}</td>

                                    @foreach($rounds as $round)
                                    <td style="
                @if(\App\ResultsChecker::getResult($team->id, $round->id) == -1) background:red
                @endif
                ">{{ \App\ResultsChecker::getResult($team->id, $round->id) }}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
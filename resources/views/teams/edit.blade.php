<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('teams.add')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card">
                        <div class="card-body">
                            <form autocomplete="off" method="POST" action="{{route('teams.save', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$team->id])}}">
                                @csrf

                                {{-- Team Number --}}
                                <div class="form-group row" style="margin-top:10px">
                                    <label for="teamnumber" class="col-md-4 col-form-label text-md-right">{{ __('teams.number') }}</label>
                                    <div class="col-md-6">
                                        <input id="teamnumber" type="text" class="form-control" name="teamnumber" value="{{$team->teamNumber}}" required>
                                    </div>
                                </div>

                                {{-- Team name --}}
                                <div class="form-group row" style="margin-top:10px">
                                    <label for="teamname" class="col-md-4 col-form-label text-md-right">{{ __('teams.name') }}</label>
                                    <div class="col-md-6">
                                        <input id="teamname" type="text" class="form-control" name="teamname" value="{{$team->teamname}}" required>
                                    </div>
                                </div>

                                {{-- Affiliate --}}
                                <div class="form-group row" style="margin-top:10px">
                                    <label for="affiliate" class="col-md-4 col-form-label text-md-right">{{ __('teams.affiliate') }}</label>
                                    <div class="col-md-6">
                                        <input id="affiliate" type="text" class="form-control" name="affiliate" value="{{$team->affiliate}}">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary" style="margin-top:10px">
                                    {{ __('settings.submit') }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
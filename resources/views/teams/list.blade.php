<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <div class="fll-beside">{{__('teams.list')}}</div>
            @can('teams.add')
            <div class="fll-right fll-beside"> <a href="{{route('teams.add', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}" class="btn btn-default btn-success"> <i class="fas fa-plus-circle"></i> {{__('general.add')}} </a></div>
            @endcan
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card">
                        <div class="card-body">

                            <table data-toggle="table" data-search="true">
                                <thead>
                                    <tr>
                                        <th data-sortable="true">{{__('teams.ID')}}</th>
                                        <th data-sortable="true">{{__('teams.number')}}</th>
                                        <th data-sortable="true">{{__('teams.name')}}</th>
                                        <th data-sortable="true">{{__('teams.affiliate')}}</th>
                                        @can('teams.edit')
                                        <th>{{__('teams.edit')}}</th>
                                        <th>{{__('teams.delete')}}</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($teams as $team)
                                    <tr>
                                        <td>{{$team->id}}</td>
                                        <td>{{$team->teamNumber}}</td>
                                        <td>{{$team->teamname}}</td>
                                        <td>{{$team->affiliate}}</td>
                                        @can('teams.edit')
                                        <td>

                                            <a href="{{route('teams.edit', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$team->id]) }}" class="btn btn-default btn-info">
                                                <i class="fas fa-eye"></i>
                                            </a>

                                        </td>

                                        <td>

                                            <a href="{{route('teams.delete.get', ['locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$team->id]) }}" class="btn btn-default btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </a>

                                        </td>

                                        @endcan
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
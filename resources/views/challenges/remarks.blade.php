<div class="card scoreform-spacer">
    <div class="card-header">
        <div class="fll-beside">{{__('scoreform-general.remarks')}}</div>
    </div>

    <div class="card-body">
    <textarea rows="3" cols="100" id="remarks" name="remarks"
            placeholder="{{__('scoreform-general.remarks')}}?">{{$game->remarks}}
    </textarea>
    </div>
</div>
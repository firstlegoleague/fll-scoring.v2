{{-- Mission 03 | Unload Cargo Plane --}}
<div class="card scoreform-spacer">
    <div id="M03" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M03-name')}}</div>
        <div id="M03_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="alert-danger" id="M03-error"></div>

        {{__('challenge2021.M03-scoring1')}}

        <div class="fll-beside fll-right">
            <input @if($game->M03_1 == 0)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_no" name="M03_1" value="0">
            <label onclick="js_M03();" for="m03_1_no">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M03_1 == 1)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_yes" name="M03_1" value="1">
            <label onclick="js_M03();" for="m03_1_yes">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M03-scoring2')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M03_2 == 0)
                checked
                @endif
                required onclick="js_M03();" type="radio" id="m03_2_no" name="M03_2" value="0">
            <label onclick="js_M03();" for="m03_2_no">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M03_2 == 1)
                checked
                @endif
                required onclick="js_M03();" type="radio" id="m03_2_yes" name="M03_2" value="1">
            <label onclick="js_M03();" for="m03_2_yes">{{__('challenge2021.yes')}}</label>
        </div>
    </div>
</div>
{{-- Mission 15 | Load Cargo --}}
<div class="card scoreform-spacer">
    <div id="M15" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M15-name')}}</div>
        <div id="M15_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="alert-danger" id="M15-error"></div>

        {{__('challenge2021.M15-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M15_1 == 0)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_1_0" name="M15_1" value="0">
            <label onclick="js_M15();" for="M15_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_1 == 1)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_1_1" name="M15_1" value="1">
            <label onclick="js_M15();" for="M15_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_1 == 2)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_1_2" name="M15_1" value="2">
            <label onclick="js_M15();" for="M15_1_2">{{__('challenge2021.M15-more')}}</label>
        </div>
        <div class="fll-spacer"></div>
        {{__('challenge2021.M15-scoring2')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M15_2 == 0)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_2_0" name="M15_2" value="0">
            <label onclick="js_M15();" for="M15_2_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_2 == 1)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_2_1" name="M15_2" value="1">
            <label onclick="js_M15();" for="M15_2_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_2 == 2)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_2_2" name="M15_2" value="2">
            <label onclick="js_M15();" for="M15_2_2">{{__('challenge2021.M15-more')}}</label>
        </div>
        <div class="fll-spacer"></div>
        {{__('challenge2021.M15-scoring3')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M15_3 == 0)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_3_0" name="M15_3" value="0">
            <label onclick="js_M15();" for="M15_3_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_3 == 1)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_3_1" name="M15_3" value="1">
            <label onclick="js_M15();" for="M15_3_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M15_3 == 2)
                checked
                @endif
                required onclick="js_M15();" type="radio" id="M15_3_2" name="M15_3" value="2">
            <label onclick="js_M15();" for="M15_3_2">{{__('challenge2021.M15-more')}}</label>
        </div>

    </div>
</div>

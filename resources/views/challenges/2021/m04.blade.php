{{-- Mission 04 | Transportation Journey --}}
<div class="card scoreform-spacer">
    <div id="M04" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M04-name')}}</div>
        <div id="M04_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        {{__('challenge2021.M04-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M04_1 == 0)
                checked
                @endif
                required onclick="js_M04();" type="radio" id="m04_1_no" name="M04_1" value="0">
            <label onclick="js_M04();" for="m04_1_no">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M04_1 == 1)
                    checked
                    @endif
                    required onclick="js_M04();" type="radio" id="m04_1_yes" name="M04_1" value="1">
            <label onclick="js_M04();" for="m04_1_yes">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M04-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M04_2 == 0)
                    checked
                    @endif
                    required onclick="js_M04();" type="radio" id="m04_2_nee" name="M04_2" value="0">
            <label onclick="js_M04();" for="m04_2_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M04_2 == 1)
                    checked
                    @endif
                    required onclick="js_M04();" type="radio" id="m04_2_ja" name="M04_2" value="1">
            <label onclick="js_M04();" for="m04_2_ja">{{__('challenge2021.yes')}}</label>
        </div>
    </div>
</div>
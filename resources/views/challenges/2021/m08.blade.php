{{-- Mission 08 | Air Drop --}}
<div class="card scoreform-spacer">
    <div id="M08" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M08-name')}}</div>
        <div id="M08_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="alert-danger hideError alert fll-error" id="M08-error">{{__('challenge2021.M08-error')}}</div>

    <div class="card-body radio-toolbar">

        {{__('challenge2021.M08-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M08_1 == 0)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_1_nee" name="M08_1" value="0">
            <label onclick="js_M08();" for="m08_1_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M08_1 == 1)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_1_ja" name="M08_1" value="1">
            <label onclick="js_M08();" for="m08_1_ja">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M08-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M08_2 == 0)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_2_nee" name="M08_2" value="0">
            <label onclick="js_M08();" for="m08_2_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M08_2 == 1)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_2_ja" name="M08_2" value="1">
            <label onclick="js_M08();" for="m08_2_ja">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M08-scoring3')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M08_3 == 0)
                checked
                @endif
                required onclick="js_M08();" type="radio" id="m08_3_nee" name="M08_3" value="0">
            <label onclick="js_M08();" for="m08_3_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M08_3 == 1)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_3_ja" name="M08_3" value="1">
            <label onclick="js_M08();" for="m08_3_ja">{{__('challenge2021.yes')}}</label>
        </div>
    </div>
</div>
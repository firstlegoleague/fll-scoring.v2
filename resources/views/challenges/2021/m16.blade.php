{{-- Mission 16 | Cargo Connect --}}
<div class="card scoreform-spacer">
    <div id="M16" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M16-name')}}</div>
        <div id="M16_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="alert-danger hideError alert fll-error"
            id="M16-error">{{__('challenge2021.M16-error')}}</div>

    <div class="card-body radio-toolbar">
        {{__('challenge2021.M16-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M16_1 == 0)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_0" name="M16_1" value="0">
            <label onclick="js_M16();" for="M16_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 1)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_1" name="M16_1" value="1">
            <label onclick="js_M16();" for="M16_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 2)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_2" name="M16_1" value="2">
            <label onclick="js_M16();" for="M16_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 3)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_3" name="M16_1" value="3">
            <label onclick="js_M16();" for="M16_1_3">3</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 4)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_4" name="M16_1" value="4">
            <label onclick="js_M16();" for="M16_1_4">4</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 5)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_5" name="M16_1" value="5">
            <label onclick="js_M16();" for="M16_1_5">5</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 6)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_6" name="M16_1" value="6">
            <label onclick="js_M16();" for="M16_1_6">6</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 7)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_7" name="M16_1" value="7">
            <label onclick="js_M16();" for="M16_1_7">7</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_1 == 8)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_1_8" name="M16_1" value="8">
            <label onclick="js_M16();" for="M16_1_8">8</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2021.M16-scoring2')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M16_2 == 0)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_0" name="M16_2" value="0">
            <label onclick="js_M16();" for="M16_2_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 1)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_1" name="M16_2" value="1">
            <label onclick="js_M16();" for="M16_2_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 2)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_2" name="M16_2" value="2">
            <label onclick="js_M16();" for="M16_2_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 3)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_3" name="M16_2" value="3">
            <label onclick="js_M16();" for="M16_2_3">3</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 4)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_4" name="M16_2" value="4">
            <label onclick="js_M16();" for="M16_2_4">4</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 5)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_5" name="M16_2" value="5">
            <label onclick="js_M16();" for="M16_2_5">5</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 6)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_6" name="M16_2" value="6">
            <label onclick="js_M16();" for="M16_2_6">6</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 7)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_7" name="M16_2" value="7">
            <label onclick="js_M16();" for="M16_2_7">7</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_2 == 8)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_2_8" name="M16_2" value="8">
            <label onclick="js_M16();" for="M16_2_8">8</label>
        </div>
        <div class="fll-spacer"></div>
        {{__('challenge2021.M16-scoring3')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M16_3 == 0)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_3_nee" name="M16_3" value="0">
            <label onclick="js_M16();" for="M16_3_nee">{{__('challenge2021.no')}}</label>

            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_3 == 1)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_3_ja" name="M16_3" value="1">
            <label onclick="js_M16();" for="M16_3_ja">{{__('challenge2021.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2021.M16-scoring4')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M16_4 == 0)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="m16_4_nee" name="M16_4" value="0">
            <label onclick="js_M16();" for="m16_4_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_4 == 1)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="m16_4_ja" name="M16_4" value="1">
            <label onclick="js_M16();" for="m16_4_ja">{{__('challenge2021.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2021.M16-scoring5')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M16_5 == 0)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_0" name="M16_5" value="0">
            <label onclick="js_M16();" for="M16_5_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 1)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_1" name="M16_5" value="1">
            <label onclick="js_M16();" for="M16_5_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 2)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_2" name="M16_5" value="2">
            <label onclick="js_M16();" for="M16_5_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 3)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_3" name="M16_5" value="3">
            <label onclick="js_M16();" for="M16_5_3">3</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 4)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_4" name="M16_5" value="4">
            <label onclick="js_M16();" for="M16_5_4">4</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 5)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_5" name="M16_5" value="5">
            <label onclick="js_M16();" for="M16_5_5">5</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M16_5 == 6)
                checked
                @endif
                required onclick="js_M16();" type="radio" id="M16_5_6" name="M16_5" value="6">
            <label onclick="js_M16();" for="M16_5_6">6</label>

        </div>
    </div>
</div>
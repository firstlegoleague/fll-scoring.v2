{{-- Mission 05 | Switch Engine --}}
<div class="card scoreform-spacer">
    <div id="M05" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M05-name')}}</div>
        <div id="M05_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        {{__('challenge2021.M05-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M05_1 == 0)
                    checked
                    @endif required onclick="js_M05();" type="radio" id="m05_1_nee" name="M05_1"
                    value="0">
            <label onclick="js_M05();" for="m05_1_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M05_1 == 1)
                    checked
                    @endif
                    required onclick="js_M05();" type="radio" id="m05_1_ja" name="M05_1" value="1">
            <label onclick="js_M05();" for="m05_1_ja">{{__('challenge2021.yes')}}</label>
        </div>
    </div>
</div>
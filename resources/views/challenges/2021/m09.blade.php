{{-- Mission 09 | Train Tracks --}}
<div class="card scoreform-spacer">
    <div id="M09" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M09-name')}}</div>
        <div id="M09_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="alert-danger hideError" id="M09-error">{{__('challenge2021.M09-error')}}</div>

        {{__('challenge2021.M09-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M09_1 == 0)
                    checked
                    @endif
                    required onclick="js_M09();" type="radio" id="m09_1_nee" name="M09_1" value="0">
            <label onclick="js_M09();" for="m09_1_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M09_1 == 1)
                    checked
                    @endif
                    required onclick="js_M09();" type="radio" id="m09_1_ja" name="M09_1" value="1">
            <label onclick="js_M09();" for="m09_1_ja">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M09-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M09_2 == 0)
                    checked
                    @endif
                    required onclick="js_M09();" type="radio" id="m09_2_nee" name="M09_2" value="0">
            <label onclick="js_M09();" for="m09_2_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M09_2 == 1)
                    checked
                    @endif
                    required onclick="js_M09();" type="radio" id="m09_2_ja" name="M09_2" value="1">
            <label onclick="js_M09();" for="m09_2_ja">{{__('challenge2021.yes')}}</label>
        </div>


    </div>
</div>
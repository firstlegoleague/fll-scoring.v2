{{-- Mission 14 | Bridge --}}
<div class="card scoreform-spacer">
    <div id="M14" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M14-name')}}</div>
        <div id="M14_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="alert-danger" id="M14-error"></div>

        {{__('challenge2021.M14-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M14_1 == 0)
                checked
                @endif
                required onclick="js_M14();" type="radio" id="M14_1_0" name="M14_1" value="0">
            <label onclick="js_M14();" for="M14_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M14_1 == 1)
                checked
                @endif
                required onclick="js_M14();" type="radio" id="M14_1_1" name="M14_1" value="1">
            <label onclick="js_M14();" for="M14_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M14_1 == 2)
                checked
                @endif
                required onclick="js_M14();" type="radio" id="M14_1_2" name="M14_1" value="2">
            <label onclick="js_M14();" for="M14_1_2">2</label>

        </div>

    </div>
</div>
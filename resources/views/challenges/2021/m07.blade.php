{{-- Mission 07 | Unload Cargo Ship --}}
<div class="card scoreform-spacer">
    <div id="M07" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M07-name')}}</div>
        <div id="M07_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="alert-danger" id="M07-error"></div>

        {{__('challenge2021.M07-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M07_1 == 0)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_nee" name="M07_1" value="0">
            <label onclick="js_M07();" for="m07_1_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M07_1 == 1)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_ja" name="M07_1" value="1">
            <label onclick="js_M07();" for="m07_1_ja">{{__('challenge2021.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>

        {{__('challenge2021.M07-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M07_2 == 0)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_2_nee" name="M07_2" value="0">
            <label onclick="js_M07();" for="m07_2_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M07_2 == 1)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_2_ja" name="M07_2" value="1">
            <label onclick="js_M07();" for="m07_2_ja">{{__('challenge2021.yes')}}</label>
        </div>

    </div>
</div>
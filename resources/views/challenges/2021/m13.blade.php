{{-- Mission 13 | Platooning Trucks --}}
<div class="card scoreform-spacer">
    <div id="M13" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M13-name')}}</div>
        <div id="M13_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        {{__('challenge2021.M13-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M13_1 == 0)
                checked
                @endif
                required onclick="js_M13();" type="radio" id="m13_nee" name="M13_1" value="0">
            <label onclick="js_M13();" for="m13_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M13_1 == 1)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_ja" name="M13_1" value="1">
            <label onclick="js_M13();" for="m13_ja">{{__('challenge2021.yes')}}</label>
        </div>
        <div class="fll-spacer"></div>

        {{__('challenge2021.M13-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M13_2 == 0)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_2_nee" name="M13_2" value="0">
            <label onclick="js_M13();" for="m13_2_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M13_2 == 1)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_2_ja" name="M13_2" value="1">
            <label onclick="js_M13();" for="m13_2_ja">{{__('challenge2021.yes')}}</label>
        </div>

    </div>
</div>
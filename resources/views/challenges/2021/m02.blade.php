{{-- Mission 02 | Unused Capacity --}}
<div class="card scoreform-spacer">
    <div id="M02" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M02-name')}}</div>
        <div id="M02_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2021.M02-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M02_1 == 0)
                checked
                @endif
                required onclick="js_M02();" type="radio" id="m02_1_no" name="M02_1" value="0">
            <label onclick="js_M02();" for="m02_1_no">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M02_1 == 1)
                    checked
                    @endif
                    required onclick="js_M02();" type="radio" id="m02_1_yes" name="M02_1" value="1">
            <label onclick="js_M02();" for="m02_1_yes">{{__('challenge2021.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>

        {{__('challenge2021.M02-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M02_2 == 0)
                    checked
                    @endif
                    required onclick="js_M02();" type="radio" id="m02_2_0" name="M02_2" value="0">
            <label onclick="js_M02();" for="m02_2_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M02_2 == 1)
                    checked
                    @endif
                    required onclick="js_M02();" type="radio" id="m02_2_1-5" name="M02_2" value="1">
            <label onclick="js_M02();" for="m02_2_1-5">1-5</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M02_2 == 2)
                    checked
                    @endif
                    required onclick="js_M02();" type="radio" id="m02_2_6" name="M02_2" value="2">
            <label onclick="js_M02();" for="m02_2_6">6</label>
        </div>

    </div>


</div>
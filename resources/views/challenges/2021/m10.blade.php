{{-- Mission 10 | Sorting Center --}}
<div class="card scoreform-spacer">
    <div id="M10" class="card-header">
        <div class="fll-beside">{{__('challenge2021.M10-name')}}</div>
        <div id="M10_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">

        {{__('challenge2021.M10-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M10_1 == 0)
                    checked
                    @endif
                    required onclick="js_M10();" type="radio" id="m10_1_nee" name="M10_1" value="0">
            <label onclick="js_M10();" for="m10_1_nee">{{__('challenge2021.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M10_1 == 1)
                    checked
                    @endif
                    required onclick="js_M10();" type="radio" id="m10_1_ja" name="M10_1" value="1">
            <label onclick="js_M10();" for="m10_1_ja">{{__('challenge2021.yes')}}</label>
        </div>

    </div>
</div>
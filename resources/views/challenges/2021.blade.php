<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('challenge2021.title') }} | {{__('scoreform-general.scoreform')}}
        </h2>
    </x-slot>


    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (session()->has('successSaved'))
            <div class="alert alert-success" role="alert">
                {{__('scoreform-general.saved_successfull')}}
            </div>
            @endif

            <form autocomplete="off" method="POST" action="{{route('scoreform.post.2021', ['locale' => $locale])}}">
                @csrf

                @can('scoreform.save')
                @include('challenges.teaminfo')
                @endcan
                @include('challenges.2021.m00')
                @include('challenges.2021.m01')
                @include('challenges.2021.m02')
                @include('challenges.2021.m03')
                @include('challenges.2021.m04')
                @include('challenges.2021.m05')
                @include('challenges.2021.m06')
                @include('challenges.2021.m07')
                @include('challenges.2021.m08')
                @include('challenges.2021.m09')
                @include('challenges.2021.m10')
                @include('challenges.2021.m11')
                @include('challenges.2021.m12')
                @include('challenges.2021.m13')
                @include('challenges.2021.m14')
                @include('challenges.2021.m15')
                @include('challenges.2021.m16')
                @include('challenges.2021.m17')
                @include('challenges.gracious')

                @can('scoreform.save')
                    {{-- Returns --}}
                    <div class="card scoreform-spacer">
                        <div id="notition" class="card-header">
                            <div class="fll-beside">{{__('challenge2021.return')}}</div>
                        </div>

                        <div class="card-body r">
                            {{__('challenge2021.returns')}}
                        </div>
                    </div>


                    {{-- Remarks --}}
                    @include('challenges.remarks')
                @endcan

                <div class="card scoreform-spacer">
                        <div id="score" class="card-header">
                            <div class="fll-beside">{{__('scoreform-general.submit')}}</div>
                            <div id="total_pt" class="fll-beside fll-right">0 pt</div>
                        </div>

                        <div class="card-body radio-toolbar">
                            <input class="button button1" type="button" onclick="calcScore()"
                                   value="{{__('scoreform-general.checkForm')}}">
                        </div>

                        <input type="hidden" name="totalScore" id="total_pt_form">

                        @can('scoreform.save')
                            <div class="card-body radio-toolbar">
                                <input class="button button1 button_disabled" type="submit" value="{{__('scoreform-general.submit')}}" id="submitButton"
                                       disabled>
                            </div>
                        @endcan
                    </div>



             </form>
    </div>

    <script src="{{ asset('js/challenge2021.js') }}" defer></script>

</x-app-layout>



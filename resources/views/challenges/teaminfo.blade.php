<div class="card" xmlns="http://www.w3.org/1999/html">
    <div class="card-header">{{__('scoreform-general.gameInfo')}}</div>
    <div class="card-body">

        <input type="hidden" id="game_id" name="game_id" value="{{$game->id}}">

        {{-- The team selection box --}}
        <div class="form-group row ">
            <label class="col-md-2 col-form-label text-md-right" for="team">{{__('scoreform-general.team')}}</label>
            <div class="col-md-8">
                <input required class="form-control" list="teams" id="team" name="team"

                @if($game->id != null)
                    value="{{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamNumber}} {{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamname}}"
                @endif


                />
                <datalist id="teams">
                    @foreach($teams as $team)
                        <option
                            value="{{$team->teamNumber}} {{$team->teamname}}">
                        </option>
                    @endforeach
                </datalist>
            </div>
        </div>

        {{-- The round selection --}}
        <div class="form-group row" style="margin-top: 10px">
            <label class="col-md-2 col-form-label text-md-right" for="team">{{__('scoreform-general.round')}}</label>
            <div class="col-md-8">
                <select required class="form-control" list="rounds" id="round" name="round"/>
                @foreach($rounds as $round)
                        <option
                            @if($game->roundID == $round->id)
                                selected
                            @elseif(Auth::user()->lastRound == $round->id)
                                selected
                            @endif
                            value="{{$round->id}}">{{$round->round}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        {{-- The Table selection --}}
        <div class="form-group row" style="margin-top: 10px">
            <label class="col-md-2 col-form-label text-md-right" for="table">{{__('scoreform-general.table')}}</label>
            <div class="col-md-8">
                <select required class="form-control" list="table" id="table" name="table"/>
                @foreach($tables as $table)
                        <option
                            @if(Auth::user()->lastTable == $table->id)
                                selected
                            @elseif($game->tableID == $table->id)
                                selected
                            @endif
                            value="{{$table->id}}">{{$table->table}}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>
</div>

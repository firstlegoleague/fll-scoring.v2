{{-- Gracious Professialism  --}}
<div class="card scoreform-spacer">
    <div id="GP" class="card-header">
        <div class="fll-beside">{{__('challenge2021.gp-text')}}</div>
    </div>

    <div class="card-body radio-toolbar">

        <div class="fll-beside fll-right">
            <input @if($game->GP_1 == 2)
                    checked
                    @endif
                    required onclick="js_GP();" type="radio" id="GP_1_0" name="GP_1" value="2">
            <label onclick="js_GP();" for="GP_1_0">{{__('challenge2021.developing')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->GP_1 == 3)
                checked
                @endif required onclick="js_GP();" type="radio" id="GP_1_1" name="GP_1"
                value="3">
            <label onclick="js_GP();" for="GP_1_1">{{__('challenge2021.accomplished')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->GP_1 == 4)
                checked
                @endif required onclick="js_GP();" type="radio" id="GP_1_2" name="GP_1"
                value="4">
            <label onclick="js_GP();" for="GP_1_2">{{__('challenge2021.exceeds')}}</label>

        </div>

    </div>
</div>
{{-- Mission 17 | Precision --}}
<div class="card scoreform-spacer">
    <div id="M17" class="card-header">
        <div class="fll-beside">{{__('challenge2022.precision-name')}}</div>
        <div id="M17_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.precision-scoring')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->precision == 0)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_0" name="M17_1" value="0">
            <label onclick="js_M17();" for="M17_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 1)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_1" name="M17_1" value="1">
            <label onclick="js_M17();" for="M17_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 2)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_2" name="M17_1" value="2">
            <label onclick="js_M17();" for="M17_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 3)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_3" name="M17_1" value="3">
            <label onclick="js_M17();" for="M17_3">3</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 4)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_4" name="M17_1" value="4">
            <label onclick="js_M17();" for="M17_4">4</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 5)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_5" name="M17_1" value="5">
            <label onclick="js_M17();" for="M17_5">5</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->precision == 6)
                checked
                @endif
                required onclick="js_M17();" type="radio" id="M17_6" name="M17_1" value="6">
            <label onclick="js_M17();" for="M17_6">6</label>
        </div>

    </div>
</div>
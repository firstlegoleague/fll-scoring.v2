{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M14" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M14-name')}}</div>
        <div id="M14_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M14-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M14_1 == 0)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_1_0" name="M14_1" value="0">
            <label onclick="js_M14();" for="m14_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M14_1 == 1)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_1_1" name="M14_1" value="1">
            <label onclick="js_M14();" for="m14_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M14_1 == 2)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_1_2" name="M14_1" value="2">
            <label onclick="js_M14();" for="m14_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M14_1 == 3)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_1_3" name="M14_1" value="3">
            <label onclick="js_M14();" for="m14_1_3">3</label>
        </div>

        <div class="fll-spacer"></div>

        {{__('challenge2022.M14-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M14_2 == 0)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_2_nee" name="M14_2" value="0">
            <label onclick="js_M14();" for="m14_2_nee">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M14_2 == 1)
                    checked
                    @endif
                    required onclick="js_M14();" type="radio" id="m14_2_ja" name="M14_2" value="1">
            <label onclick="js_M14();" for="m14_2_ja">{{__('challenge2022.yes')}}</label>
        </div>

    </div>

</div>

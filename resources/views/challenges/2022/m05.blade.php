{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M05" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M05-name')}}</div>
        <div id="M05_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M05-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M05_1 == 0)
                    checked
                    @endif
                    required onclick="js_M05();" type="radio" id="m05_1_no" name="M05_1" value="0">
            <label onclick="js_M05();" for="m05_1_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M05_1 == 1)
                    checked
                    @endif
                    required onclick="js_M05();" type="radio" id="m05_1_yes" name="M05_1" value="1">
            <label onclick="js_M05();" for="m05_1_yes">{{__('challenge2022.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2022.M05-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M05_2 == 0)
                    checked
                    @endif
                    required onclick="js_M05();" type="radio" id="m05_2_no" name="M05_2" value="0">
            <label onclick="js_M05();" for="m05_2_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M05_2 == 1)
                    checked
                    @endif
                    required onclick="js_M05();" type="radio" id="m05_2_yes" name="M05_2" value="1">
            <label onclick="js_M05();" for="m05_2_yes">{{__('challenge2022.yes')}}</label>
        </div>

        
    </div>
</div>

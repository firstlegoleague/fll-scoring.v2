{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M13" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M13-name')}}</div>
        <div id="M13_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M13-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M13_1 == 0)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_1_0" name="M13_1" value="0">
            <label onclick="js_M13();" for="m13_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M13_1 == 1)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_1_1" name="M13_1" value="1">
            <label onclick="js_M13();" for="m13_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M13_1 == 2)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_1_2" name="M13_1" value="2">
            <label onclick="js_M13();" for="m13_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M13_1 == 3)
                    checked
                    @endif
                    required onclick="js_M13();" type="radio" id="m13_1_3" name="M13_1" value="3">
            <label onclick="js_M13();" for="m13_1_3">3+</label>
        </div>

    </div>

</div>

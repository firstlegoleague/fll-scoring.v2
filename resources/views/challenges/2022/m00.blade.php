{{-- Mission 00 | Inspectiones --}}
<div class="card scoreform-spacer">
    <div id="M00" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M00-name')}}</div>
        <div id="M00_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M00-scoring1')}}
        <div class="fll-beside fll-right">
            <input
                @if($game->M00_1 == 0)
                    checked
                @endif
                required onclick="js_M00();" type="radio" id="m00_no" name="M00_1" value="0">
            <label onclick="js_M00();" for="m00_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input
                @if($game->M00_1 == 1)
                    checked
                @endif
                required onclick="js_M00();" type="radio" id="m00_yes" name="M00_1" value="1">
            <label onclick="js_M00();" for="m00_yes">{{__('challenge2022.yes')}}</label>

        </div>
    </div>
</div>

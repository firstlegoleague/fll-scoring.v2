{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M12" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M12-name')}}</div>
        <div id="M12_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M12-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M12_1 == 0)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_1_0" name="M12_1" value="0">
            <label onclick="js_M12();" for="m12_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M12_1 == 1)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_1_1" name="M12_1" value="1">
            <label onclick="js_M12();" for="m12_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M12_1 == 2)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_1_2" name="M12_1" value="2">
            <label onclick="js_M12();" for="m12_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M12_1 == 3)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_1_3" name="M12_1" value="3">
            <label onclick="js_M12();" for="m12_1_3">3</label>
        </div>

        <div class="fll-spacer"></div>

        {{__('challenge2022.M12-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M12_2 == 0)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_2_0" name="M12_2" value="0">
            <label onclick="js_M12();" for="m12_2_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M12_2 == 1)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_2_1" name="M12_2" value="1">
            <label onclick="js_M12();" for="m12_2_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M12_2 == 2)
                    checked
                    @endif
                    required onclick="js_M12();" type="radio" id="m12_2_2" name="M12_2" value="2">
            <label onclick="js_M12();" for="m12_2_2">2</label>
        </div>

    </div>

</div>

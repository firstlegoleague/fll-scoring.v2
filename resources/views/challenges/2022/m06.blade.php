<div class="card scoreform-spacer">

    <div id="M06" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M06-name')}}</div>
        <div id="M06_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M06-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M06_1 == 0)
                    checked
                    @endif
                    required onclick="js_M06();" type="radio" id="m06_1_no" name="M06_1" value="0">
            <label onclick="js_M06();" for="m06_1_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M06_1 == 1)
                    checked
                    @endif
                    required onclick="js_M06();" type="radio" id="m06_1_yes" name="M06_1" value="1">
            <label onclick="js_M06();" for="m06_1_yes">{{__('challenge2022.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2022.M06-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M06_2 == 0)
                    checked
                    @endif
                    required onclick="js_M06();" type="radio" id="m06_2_no" name="M06_2" value="0">
            <label onclick="js_M06();" for="m06_2_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M06_2 == 1)
                    checked
                    @endif
                    required onclick="js_M06();" type="radio" id="m06_2_yes" name="M06_2" value="1">
            <label onclick="js_M06();" for="m06_2_yes">{{__('challenge2022.yes')}}</label>
        </div>

        
    </div>
</div>

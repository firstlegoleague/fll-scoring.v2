{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M01" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M01-name')}}</div>
        <div id="M01_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M01-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M01_1 == 0)
                    checked
                    @endif
                    required onclick="js_M01();" type="radio" id="m01_1_no" name="M01_1" value="0">
            <label onclick="js_M01();" for="m01_1_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M01_1 == 1)
                    checked
                    @endif
                    required onclick="js_M01();" type="radio" id="m01_1_yes" name="M01_1" value="1">
            <label onclick="js_M01();" for="m01_1_yes">{{__('challenge2022.yes')}}</label>
        </div>
    </div>

    <div class="card-body">{!! __('challenge2022.M01-remark') !!}</div>

</div>

{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M11" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M11-name')}}</div>
        <div id="M11_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">


    {{__('challenge2022.M11-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M11_1 == 0)
                    checked
                    @endif
                    required onclick="js_M11();" type="radio" id="m11_1_nee" name="M11_1" value="0">
            <label onclick="js_M11();" for="m11_1_nee">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M11_1 == 1)
                    checked
                    @endif
                    required onclick="js_M11();" type="radio" id="m11_1_ja" name="M11_1" value="1">
            <label onclick="js_M11();" for="m11_1_ja">{{__('challenge2022.yes')}}</label>
        </div>

    </div>

</div>

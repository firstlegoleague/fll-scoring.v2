{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M03" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M03-name')}}</div>
        <div id="M03_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M03-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M03_1 == 0)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_0" name="M03_1" value="0">
            <label onclick="js_M03();" for="m03_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M03_1 == 1)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_1" name="M03_1" value="1">
            <label onclick="js_M03();" for="m03_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M03_1 == 2)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_2" name="M03_1" value="2">
            <label onclick="js_M03();" for="m03_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M03_1 == 3)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_1_3" name="M03_1" value="3">
            <label onclick="js_M03();" for="m03_1_3">3+</label>
        </div>

        <div class="fll-spacer"></div>

        {{__('challenge2022.M03-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M03_2 == 0)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_2_nee" name="M03_2" value="0">
            <label onclick="js_M03();" for="m03_2_nee">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M03_2 == 1)
                    checked
                    @endif
                    required onclick="js_M03();" type="radio" id="m03_2_ja" name="M03_2" value="1">
            <label onclick="js_M03();" for="m03_2_ja">{{__('challenge2022.yes')}}</label>
        </div>

    </div>

</div>

<div class="card scoreform-spacer">

    <div id="M08" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M08-name')}}</div>
        <div id="M08_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M08-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M08_1 == 0)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_1_no" name="M08_1" value="0">
            <label onclick="js_M08();" for="m08_1_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M08_1 == 1)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_1_yes" name="M08_1" value="1">
            <label onclick="js_M08();" for="m08_1_yes">{{__('challenge2022.yes')}}</label>
        </div>

        <div class="fll-spacer"></div>
        {{__('challenge2022.M08-scoring2')}}
        <div class="fll-beside fll-right">
            <input @if($game->M08_2 == 0)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_2_no" name="M08_2" value="0">
            <label onclick="js_M08();" for="m08_2_no">{{__('challenge2022.no')}}</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M08_2 == 1)
                    checked
                    @endif
                    required onclick="js_M08();" type="radio" id="m08_2_yes" name="M08_2" value="1">
            <label onclick="js_M08();" for="m08_2_yes">{{__('challenge2022.yes')}}</label>
        </div>

        
    </div>
</div>

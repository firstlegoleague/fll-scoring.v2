{{-- Mission 01 | Innovation Project --}}
<div class="card scoreform-spacer">

    <div id="M07" class="card-header">
        <div class="fll-beside">{{__('challenge2022.M07-name')}}</div>
        <div id="M07_pt" class="fll-beside fll-right">0 pt</div>
    </div>

    <div class="card-body radio-toolbar">
        {{__('challenge2022.M07-scoring1')}}
        <div class="fll-beside fll-right">
            <input @if($game->M07_1 == 0)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_0" name="M07_1" value="0">
            <label onclick="js_M07();" for="m07_1_0">0</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M07_1 == 1)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_1" name="M07_1" value="1">
            <label onclick="js_M07();" for="m07_1_1">1</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M07_1 == 2)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_2" name="M07_1" value="2">
            <label onclick="js_M07();" for="m07_1_2">2</label>
            <div class="fll-beside" style="width: 20px"></div>
            <input @if($game->M07_1 == 3)
                    checked
                    @endif
                    required onclick="js_M07();" type="radio" id="m07_1_3" name="M07_1" value="3">
            <label onclick="js_M07();" for="m07_1_3">3+</label>
        </div>
    </div>

    <!-- <div class="card-body">{!! __('challenge2022.M01-remark') !!}</div> -->

</div>

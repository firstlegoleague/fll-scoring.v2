<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12" style="margin-top:50px">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <div class="timer">
                    <div class="fll-beside">0</div><div class="fll-beside" id="timer_min">2</div><div class="fll-beside">:</div><div class="fll-beside zerohide" id="timer_zero">0</div><div class="fll-beside" id="timer_sec">30</div>
                </div>    
            </div>
            </div>

            <div class="card">
                    <div class="card-header">{{__('timer.timer-control')}}</div>

                    <div class="card-body">
                        <div class="fll-beside">
                            <button onclick="timer_start()" class="alert alert-success">{{__('timer.start')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_reset()" class="alert alert-info">{{__('timer.reset')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="timer_stop()" class="alert alert-danger">{{__('timer.stop')}}</button>
                        </div>
                    </div>
                </div>



        </div>
    </div>

    <link rel="stylesheet" href="{{ asset('css/timer.css') }}">
    <script src="https://unpkg.com/mqtt@4.3.7/dist/mqtt.min.js"></script>
    <script src="{{ asset('js/timer.js') }}" defer></script>

</x-app-layout>

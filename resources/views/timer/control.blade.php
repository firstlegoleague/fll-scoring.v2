<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('settings.title')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="card">

                        <div class="card-body">
                            <h3>Timer control</h3>
                        <div class="fll-beside">
                            <button onclick="send_start('{{\App\Models\settings::getFinalName()}}')" class="alert alert-success">{{__('timer.start')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="send_reset('{{\App\Models\settings::getFinalName()}}')" class="alert alert-info">{{__('timer.reset')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="send_stop('{{\App\Models\settings::getFinalName()}}')" class="alert alert-danger">{{__('timer.stop')}}</button>
                        </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/mqtt@4.3.7/dist/mqtt.min.js"></script>
    <script src="{{ asset('js/timer_admin.js') }}" defer></script>
</x-app-layout>
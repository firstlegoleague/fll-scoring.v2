<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <div class="fll-beside">{{__('tables.list')}}</div>
            @can('tables.add')
                <div class="fll-right fll-beside"> <a href="{{ route('tables.add_page', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}" class="btn btn-default btn-primary"> <i class="fas fa-plus-circle"></i> {{__('general.add')}} </a></div>
            @endcan
            </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card-body">


                        <table data-toggle="table" data-search="true">
                            <thead>
                                <tr>
                                    <th data-sortable="true">{{__('tables.name')}}</th>
                                    <th>{{__('general.delete')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tables as $table)
                                <tr>
                                    <td>{{$table->table}}</td>
                                    <td>
                                        @if(\App\Models\tables::hasResults($table->id) == 0)
                                        <a href="{{ route('tables.delete', [ 'locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$table->id]) }}" class="btn btn-default btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



</x-app-layout>

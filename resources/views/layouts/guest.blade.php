<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'FLL Scoring') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <script src="https://kit.fontawesome.com/b289b4ce85.js" crossorigin="anonymous"></script>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.20.0/dist/bootstrap-table.min.css">
        <script src="https://unpkg.com/bootstrap-table@1.20.0/dist/bootstrap-table.min.js"></script>

    </head>
    <body>
    <header class="bg-blue shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

            @if(\App\Models\settings::getFinalName() != "NotSet")
                <div class="fll-beside">{{ config('app.name', 'FLL Scoring') }} | {{\App\Models\settings::getFinalName()}}</div>
            @else
                <div class="fll-beside">{{ config('app.name', 'FLL Scoring') }}</div>
            @endif

            @if(\App\Models\settings::getGamePublic() == true)
                    <div class="fll-beside" style="margin-left: 25%"><a class="button button1" href="{{ route('scoreboard.guest',['locale',]) }}">{{__('menu.scoreboard-public')}}</a></div>
            @endif
        </div>
    </header>

        <div class="font-sans text-gray-900 antialiased" style="margin-top: -75px">
            <main>
            {{ $slot }}

            <div style="height: 100px"></div>
            </main>
        </div>
    </body>
</html>

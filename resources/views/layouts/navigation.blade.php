<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="shrink-0 flex items-center">
                    <a href="{{ route('dashboard',['locale'=>str_replace('_', '-', app()->getLocale())]) }}">
                        <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('dashboard',['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('dashboard')">
                        {{ __('Dashboard') }}
                    </x-nav-link>
                </div>

                @can('scoreform.save')
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('challenge', ['locale'=>str_replace('_', '-', app()->getLocale()), 'year'=>\App\Models\settings::getSeason()])" :active="request()->routeIs('challenge')">
                        {{ __('menu.newForm')}}
                    </x-nav-link>
                </div>
                @endcan

                @can('teams.view')
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('teams.list', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('teams.list')">
                        {{ __('menu.teams')}}
                    </x-nav-link>
                </div>
                @endcan

                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('scoreboard.private', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('scoreboard.private')">
                        {{ __('menu.scoreboard-public')}}
                    </x-nav-link>
                </div>

                @can('scoreform.view.scores')
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('overview.gametable', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('overview.gametable')">
                        {{ __('menu.overview-gametable')}}
                    </x-nav-link>
                </div>

                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('games.list', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('games.list')">
                        {{ __('menu.gamelist')}}
                    </x-nav-link>
                </div>
                @endcan




            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>{{ Auth::user()->name }}</div>

                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>



                    <x-slot name="content">
                        <x-dropdown-link :href="route('account.view',['locale'=>str_replace('_', '-', app()->getLocale())])">
                            {{ __('menu.account') }}
                        </x-dropdown-link>

                        @can('users.edit')
                        <x-dropdown-link :href="route('users.index')">
                            {{ __('menu.users')}}
                        </x-dropdown-link>
                        @endcan

                        @can('rounds.edit')
                        <x-dropdown-link :href="route('rounds.list', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                            {{ __('menu.rounds')}}
                        </x-dropdown-link>
                        @endcan

                        @can('tables.edit')
                        <x-dropdown-link :href="route('tables.list', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                            {{ __('menu.tables') }}
                        </x-dropdown-link>
                        @endcan

                        @can('settings.edit')
                        <x-dropdown-link :href="route('settings', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                            {{ __('menu.settings') }}
                        </x-dropdown-link>
                        @endcan

                        <x-dropdown-link :href="route('timer')">
                            {{ __('menu.timer') }}
                        </x-dropdown-link>

                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Log Out') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>



    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link :href="route('dashboard',['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-responsive-nav-link>

            @can('scoreform.save')
            <x-responsive-nav-link :href="route('challenge', ['locale'=>str_replace('_', '-', app()->getLocale()), 'year'=>\App\Models\settings::getSeason()])" :active="request()->routeIs('challenge')">
                {{ __('menu.newForm')}}
            </x-responsive-nav-link>
            @endcan

            @can('teams.view')
            <x-responsive-nav-link :href="route('teams.list', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('teams.list')">
                {{ __('menu.teams')}}
            </x-responsive-nav-link>
            @endcan

            <x-responsive-nav-link :href="route('scoreboard.public', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('scoreboard.public')">
                {{ __('menu.scoreboard-public')}}
            </x-responsive-nav-link>

            @can('scoreform.view.scores')
            <x-responsive-nav-link :href="route('overview.gametable', ['locale'=>str_replace('_', '-', app()->getLocale())])" :active="request()->routeIs('scoreboard.public')">
                {{ __('menu.overview-gametable')}}
            </x-responsive-nav-link>
            @endcan

        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="px-4">
                <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
            </div>

            <div class="mt-3 space-y-1">

                <x-responsive-nav-link :href="route('account.view', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                    {{ __('menu.account') }}
                </x-responsive-nav-link>

                @can('users.edit')
                <x-responsive-nav-link :href="route('users.index', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                    {{ __('menu.users') }}
                </x-responsive-nav-link>
                @endcan

                @can('rounds.edit')
                <x-responsive-nav-link :href="route('rounds.list', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                    {{ __('menu.rounds') }}
                </x-responsive-nav-link>
                @endcan

                @can('tables.edit')
                <x-responsive-nav-link :href="route('tables.list', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                    {{ __('menu.tables') }}
                </x-responsive-nav-link>
                @endcan

                @can('settings.edit')
                <x-responsive-nav-link :href="route('settings', ['locale'=>str_replace('_', '-', app()->getLocale())])">
                    {{ __('menu.settings') }}
                </x-responsive-nav-link>
                @endcan

                <x-responsive-nav-link :href="route('timer')">
                    {{ __('menu.timer') }}
                </x-responsive-nav-link>

                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('logout')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-responsive-nav-link>
                </form>
            </div>
        </div>
    </div>
</nav>


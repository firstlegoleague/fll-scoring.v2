<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('account.title')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="card">
            <div class="card-body">
                <table class="table table-striped">
                    <tr>
                        <td>{{__('users.id')}}</td>
                        <td>{{$user->id}}</td>
                    </tr>
                    <tr>
                        <td>{{__('account.name')}}</td>
                        <td>{{$user->name}}</td>
                    </tr>
                    <tr>
                        <td>{{__('general.email')}}</td>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <td>{{__('users.role')}}</td>
                        <td>{{$user->roles->first()->name}}</td>
                    </tr>
                    <tr>
                        <td>
                            <form action="{{ route('account.changePassword', ['locale'=>str_replace('_', '-', app()->getLocale())])  }}">
                                <input class="btn btn-primary" type="submit" value="{{__('account.changepassword')}}" />
                            </form>


                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </div>
</x-app-layout>
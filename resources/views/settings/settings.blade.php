<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('settings.title')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="card">

                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif

                            <form autocomplete="off" method="POST" action="{{ route('settings.post', ['locale'=>str_replace('_', '-', app()->getLocale())])}}">
                                @csrf
                                {{-- Final name --}}
                                <div class="form-group row">
                                    <label for="finalname" class="col-md-4 col-form-label text-md-right">{{ __('settings.finalname') }}</label>
                                    <div class="col-md-6">
                                        <input id="finalname" type="text" class="form-control" name="finalname" value="{{ App\Models\settings::all()->where('key', 'finalName')->first()->value }}" required>
                                    </div>
                                </div>

                                {{-- Season --}}
                                <div class="form-group row">
                                    <label for="season" class="col-md-4 col-form-label text-md-right">{{ __('settings.season') }}</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="season" id="season">
                                            <option value="2021" @if( \App\Models\settings::all()->where('key', 'season')->first()->value == "2021") selected @endif>2021 | {{__('seasons.2021')}}</option>
                                            <option value="2022" @if( \App\Models\settings::all()->where('key', 'season')->first()->value == "2022") selected @endif>2022 | {{__('seasons.2022')}}</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- Register enabled --}}
                                <div class="form-group row">
                                    <label for="registerEnabled" class="col-md-4 col-form-label text-md-right">{{ __('settings.registerEnabled') }}</label>
                                    <div class="col-md-6">
                                        <input style="height:100%" type="checkbox" id="registerEnabled" name="registerEnabled" @if(App\Models\settings::all()->where('key', 'register')->first()->value == "1") checked @endif class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="scorePublic" class="col-md-4 col-form-label text-md-right">{{ __('settings.scorePublic') }}</label>
                                    <div class="col-md-6">
                                        <input style="height:100%" type="checkbox" id="scorePublic" name="scorePublic" @if(App\Models\settings::all()->where('key', 'gamePublic')->first()->value == "1") checked @endif class="form-control">
                                    </div>
                                </div>

                                {{-- Submit button --}}
                                <button type="submit" class="btn btn-primary">
                                    {{ __('settings.submit') }}
                                </button>
                            </form>
                        </div>


                        <div class="card-body">
                            <h3>Timer control</h3>
                        <div class="fll-beside">
                            <button onclick="send_start('{{\App\Models\settings::getFinalName()}}')" class="alert alert-success">{{__('timer.start')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="send_reset('{{\App\Models\settings::getFinalName()}}')" class="alert alert-info">{{__('timer.reset')}}</button>
                        </div>
                        <div class="fll-beside">
                            <button onclick="send_stop('{{\App\Models\settings::getFinalName()}}')" class="alert alert-danger">{{__('timer.stop')}}</button>
                        </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/mqtt@4.3.7/dist/mqtt.min.js"></script>
    <script src="{{ asset('js/timer_admin.js') }}" defer></script>
</x-app-layout>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{__('rounds.add')}}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card">
                        <div class="card-body">
                            <form autocomplete="off" method="POST" action="/{{str_replace('_', '-', app()->getLocale()) }}/rounds/list">
                                @csrf

                                <div class="form-group row" style="margin-top:10px">
                                    <label for="round" class="col-md-4 col-form-label text-md-right">{{ __('rounds.round') }}</label>
                                    <div class="col-md-6">
                                        <input id="round" type="text" class="form-control" name="round" required>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary" style="margin-top:10px">
                                    {{ __('settings.submit') }}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

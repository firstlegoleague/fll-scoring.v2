<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <div class="fll-beside">{{__('rounds.list')}}</div>
            @can('rounds.add')
                <div class="fll-right fll-beside"> <a href="{{ route('rounds.add_page', ['locale'=>str_replace('_', '-', app()->getLocale())]) }}" class="btn btn-default btn-primary"> <i class="fas fa-plus-circle"></i> {{__('general.add')}} </a></div>
            @endcan
            </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card-body">


                        <table data-toggle="table" data-search="true">
                            <thead>
                                <tr>
                                    <th data-sortable="true">{{__('rounds.name')}}</th>
                                    <th>{{__('general.delete')}}</th>
                                    <th>{{__('general.published')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($rounds as $round)
                                <tr>
                                    <td>{{$round->round}}</td>
                                    <td>
                                        @if(\App\Models\rounds::hasResults($round->id) == 0)
                                        <a href="{{ route('rounds.delete', [ 'locale'=>str_replace('_', '-', app()->getLocale()), 'id'=>$round->id]) }}" class="btn btn-default btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                        @endif

                                    </td>

                                    <td>
                                        @if($round->public == 0)


                                        <button onclick="js_togglePublic( {{$round->id}}, {{$round->public}} )" class="float-left submit-button alert-danger alert" id="toggle_{{$round->id}}">
                                            <i class="far fa-eye-slash"></i>
                                        </button>


                                        @elseif($round->public == 1)
                                        <button onclick="js_togglePublic( {{$round->id}}, {{$round->public}} )" class="float-left submit-button alert-success alert" id="toggle_{{$round->id}}">
                                            <i class="far fa-eye"></i>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="addRound" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form autocomplete="off" method="POST" action="{{ route('rounds.add', [ 'locale'=>str_replace('_', '-', app()->getLocale()) ]) }}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('rounds.add')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="roundname" class="col-md-4 col-form-label text-md-right">{{ __('rounds.name') }}</label>
                            <div class="col-md-6">
                                <input id="roundname" type="text" class="form-control" name="roundname" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('general.close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('general.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>

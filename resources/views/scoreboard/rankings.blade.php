<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{__('general.results')}}
                        @if(\App\Models\settings::getFinalName() != "NotSet")
                            | {{\App\Models\settings::getFinalName()}}
                        @endif
                        @if(\App\Models\rounds::all()->where('public', 1)->count() != 0)
                            | {{\App\Models\rounds::all()->where('public', 1)->sortByDesc('id')->first()->round}}
                        @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="card">
                    <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th> {{__('general.rank')}}</th>
                            <th> {{__('teams.number')}}</th>
                            <th> {{__('teams.name')}}</th>
                            <th> {{__('rounds.round')}}</th>
                            <th> {{__('general.score')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($games as $game)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }} {{-- No! This is not off by one! Arrays start at zero. We are techical people right? --}}
                                </td>
                                <td>
                                    {{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamNumber}}
                                </td>
                                <td>
                                    {{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamname}}
                                </td>
                                <td>
                                    {{\App\Models\rounds::all()->where('id', $game->roundID)->first()->round}}
                                </td>
                                <td>
                                    {{$game->totalScore}}
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FLL OVERLAY SYSTEM') }}</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.png') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">


    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <script src="https://kit.fontawesome.com/b289b4ce85.js" crossorigin="anonymous"></script>

    {{--    For the search dropdown--}}
    <script src="{{ asset('js/general.js') }}" defer></script>
    {{--    <script src="{{ asset('js/screen.js') }}" defer></script>--}}
    {{--    <script src="{{ asset('js/overlay/screen.js') }}" defer></script>--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"; rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js";></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/fll.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/screen.css') }}" rel="stylesheet">


    <meta http-equiv="refresh" content=300>

</head>
<body>
<div id="app">
    <div class="container-fluid">

    <!-- Contents for larger screens -->
    <div class="d-none d-lg-block d-xl-block">
        <!-- Loading screen -->
    {{--        <div class="spinner-border text-info spinner" role="status" id="overlay_load">--}}
    {{--        </div>--}}

    <!-- The content -->
        <div id="overlay_content" onload="pageScroll();">
            <div class="row justify-content-start">
                <div class="col-12 border-info border text-center" id="title-bar">
                    <h1>
                        @if(\App\Models\settings::getFinalName() == "NotSet")
                            {{__('general.results')}}
                        @else
                            {{__('general.results')}} | {{\App\Models\settings::getFinalName()}}
                        @endif
                        @if(\App\Models\rounds::all()->where('public', 1)->count() != 0)
                            | {{\App\Models\rounds::all()->where('public', 1)->sortByDesc('id')->first()->round}}
                        @endif
                    </h1>
                </div>
            </div>

            <div class="row justify-content-center mt-5 mb-5">


                <div class="col-8 border border-primary" id="content-screen">
                    <table id="myTable">
                        <tr>
                            <td>Plaats</td>
                            <td>Team</td>
                            <td>Ronde</td>
                            <td>Score</td>
                        </tr>
                        @foreach($games as $game)
                            <tr>
                                <td>
                                    {{ $loop->index + 1 }}
                                </td>
                                <td>
                                    {{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamNumber}} {{\App\Models\teams::all()->where('id', $game->teamID)->first()->teamname}}
                                </td>
                                <td>
                                    {{\App\Models\rounds::all()->where('id', $game->roundID)->first()->round}}
                                </td>
                                <td>
                                    {{$game->totalScore}}
                                </td>

                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

        </div>
    </div>

    </div>
</div>

</body>
</html>


require('./bootstrap');

require('bootstrap-table');
import 'bootstrap-table/dist/bootstrap-table.css';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

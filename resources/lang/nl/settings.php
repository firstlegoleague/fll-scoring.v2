<?php

return array (
  'finalname' => 'Naam finale',
  'registerEnabled' => 'Gebruikers mogen registreren',
  'scorePublic' => 'Uitslag openbaar toegangelijk',
  'season' => 'Seizoen',
  'submit' => 'Opslaan',
  'title' => 'Instellingen',
);

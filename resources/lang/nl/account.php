<?php

return array (
  'changeConfirm' => 'Veranderen',
  'changepassword' => 'Wijzig je wachtwoord',
  'confirmPassword' => 'Bevestig je nieuwe wachtwoord',
  'currentPassword' => 'Huidig wachtwoord',
  'forcePassword' => 'Forceer wachtwoord',
  'name' => 'Naam',
  'newPassword' => 'Nieuw wachtwoord',
  'title' => 'Jouw account',
);

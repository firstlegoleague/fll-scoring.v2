<?php

return array (
  'add' => 'Add',
  'close' => 'Close',
  'delete' => 'Delete',
  'dontforgettosend' => 'Don\'t forget to submit the form!',
  'download' => 'Download',
  'edit' => 'Edit',
  'email' => 'Email',
  'footer-volunteer' => 'And all our volunteers!',
  'id' => 'ID',
  'name' => 'Name',
  'published' => 'Published',
  'rank' => 'Rank',
  'save' => 'Save',
  'score' => 'Score',
  'total' => 'Total',
        'results' => 'Results',
    'downloadScores' => 'Download Scores',
    'downloadRemarks' => 'Download Remarks',
);

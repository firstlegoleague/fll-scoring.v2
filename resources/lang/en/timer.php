<?php

return array (
  'reset' => 'Reset',
  'start' => 'Start',
  'stop' => 'Stop',
  'test' => 'Test the sounds (Click to expand)',
  'timer' => 'Timer',
  'timer-control' => 'Timer Control',
);

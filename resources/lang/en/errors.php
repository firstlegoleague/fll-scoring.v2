<?php

return array (
  '404_subtitle' => 'Ooops. Emmit can\'t find this page, but don\'t worry. Everything is still awesome!',
  '404_text' => 'Besides that, a lot of legofigures are currently working to find why it went wrong, and solve the issue!',
  '404_title' => 'Error 404: Page not found',
);

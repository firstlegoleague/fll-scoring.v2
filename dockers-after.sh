#!/bin/bash

sudo docker-compose exec app composer install 
sudo docker-compose exec app npm install
sudo docker-compose exec app npm run prod
sudo docker-compose exec app php artisan key:generate
sudo docker-compose exec app php artisan migrate
sudo docker-compose exec app php artisan db:seed
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::get('/{key}/teams', '\App\Http\Controllers\api\teamsController@index');
Route::post('/{key}/teams/create', '\App\Http\Controllers\api\teamsController@create');
Route::get('/{key}/teams/{id}', '\App\Http\Controllers\api\teamsController@show');
Route::post('/{key}/teams/{id}/edit', '\App\Http\Controllers\api\teamsController@edit');
Route::delete('/{key}/teams/{id}/delete', '\App\Http\Controllers\api\teamsController@delete');

Route::get('/{key}/tables', '\App\Http\Controllers\api\tablesController@index');
Route::post('/{key}/tables/create', '\App\Http\Controllers\api\tablesController@create');
Route::get('/{key}/tables/{id}', '\App\Http\Controllers\api\tablesController@show');
Route::post('/{key}/tables/{id}/edit', '\App\Http\Controllers\api\tablesController@edit');
Route::delete('/{key}/tables/{id}/delete', '\App\Http\Controllers\api\tablesController@delete');

Route::get('/{key}/rounds', '\App\Http\Controllers\api\roundsController@index');
Route::post('/{key}/rounds/create', '\App\Http\Controllers\api\roundsController@create');
Route::get('/{key}/rounds/{id}', '\App\Http\Controllers\api\roundsController@show');
Route::post('/{key}/rounds/{id}/edit', '\App\Http\Controllers\api\roundsController@edit');
Route::delete('/{key}/rounds/{id}/delete', '\App\Http\Controllers\api\roundsController@delete');

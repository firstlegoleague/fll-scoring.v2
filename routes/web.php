<?php

use Illuminate\Support\Facades\Route;
Use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('welcome');

Route::get('/timer', function () {
    return redirect()->to(route('timer.index.anchor').'#'.\App\Models\settings::getFinalName());
})->name("timer");

Route::get('/timer/index', function () {
    return view('timer.index');
})->name('timer.index.anchor');

Route::get("/timer/control", function() { return view("timer.control");})->middleware(['auth']);

Route::get('/{locale}/dashboard', function ($locale) {
    App::setLocale($locale);
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard', function(){
    return view('dashboard');
})->middleware(['auth']);

Route::get('/{locale}/challenge/{year}', function ($locale, $year) {
    App::setLocale($locale);

    if($year == '2021') {
        $game = new \App\Models\challenge2021;
    } else if(($year == '2022')){
        $game = new \App\Models\challenge2021;
    } else {
        abort(404);
    }
    $teams = \App\Models\teams::all();
    $rounds = \App\Models\rounds::all();
    $tables = \App\Models\tables::all();

    return view('challenges/'.$year , compact("game", "teams", "rounds", "tables", "locale"));
})->middleware(['auth'])->name('challenge');


Route::post("/{locale}/scoreform/2021/post", [\App\Http\Controllers\Challenge2021Controller::class, 'saveScoresheet'])->name('scoreform.post.2021')->middleware('auth');
Route::post("/{locale}/scoreform/2022/post", [\App\Http\Controllers\Challenge2022Controller::class, 'saveScoresheet'])->name('scoreform.post.2022')->middleware('auth');

// User Routes
Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', \App\Http\Controllers\UserController::class);
});

Route::group(['middleware' => []], function() {
    Route::resource('/api/{key}/key/', \App\Http\Controllers\api\apikeyController::class);
    Route::resource('/api/{key}/games/', \App\Http\Controllers\api\gamesController::class);
    Route::resource('/api/{key}/settings/', \App\Http\Controllers\api\settingsController::class);
    Route::resource('/api/{key}/users/', \App\Http\Controllers\api\usersController::class);
    Route::resource('/api/{key}/teapot/', \App\Http\Controllers\api\teapotController::class);
});


// Settings
Route::get('/{locale}/settings', '\App\Http\Controllers\SettingsController@index')->name('settings')->middleware('auth');
Route::post('/{locale}/settings', '\App\Http\Controllers\SettingsController@postSettings')->name('settings.post')->middleware('auth');


// Teams
Route::get('/{locale}/teams/list', '\App\Http\Controllers\TeamController@list')->name('teams.list')->middleware('auth');
Route::get('/{locale}/teams/add', '\App\Http\Controllers\TeamController@add_get')->middleware('auth');
Route::post('/{locale}/teams/add', '\App\Http\Controllers\TeamController@add_post')->name('teams.add')->middleware('auth');
Route::get('/{locale}/teams/edit/{id}', '\App\Http\Controllers\TeamController@edit')->name('teams.edit')->middleware('auth');
Route::post('/{locale}/teams/edit/{id}', '\App\Http\Controllers\TeamController@save')->name('teams.save')->middleware('auth');
Route::post('/{locale}/teams/delete/{id}', '\App\Http\Controllers\TeamController@delete')->name('teams.delete')->middleware('auth');
Route::get('/{locale}/teams/delete/{id}', '\App\Http\Controllers\TeamController@delete')->name('teams.delete.get')->middleware('auth');

// Rounds
Route::get('/{locale}/rounds/list', '\App\Http\Controllers\RoundController@list')->name('rounds.list')->middleware('auth');
Route::get('/{locale}/rounds/add', '\App\Http\Controllers\RoundController@add_page')->name('rounds.add_page')->middleware('auth');
Route::post('/{locale}/rounds/list', '\App\Http\Controllers\RoundController@add')->name('rounds.add')->middleware('auth');
Route::get('/{locale}/rounds/delete/{id}', '\App\Http\Controllers\RoundController@delete')->name('rounds.delete')->middleware('auth');
Route::get('rounds/{id}/public', '\App\Http\Controllers\RoundController@setPublic')->name('rounds.makePublic')->middleware('auth');
Route::get('rounds/{id}/private', '\App\Http\Controllers\RoundController@setPrivate')->name('rounds.makePrivate')->middleware('auth');

// Tables
Route::get('/{locale}/tables/list', '\App\Http\Controllers\TableController@index')->name('tables.list')->middleware('auth');
Route::get('/{locale}/tables/add', '\App\Http\Controllers\TableController@add_page')->name('tables.add_page')->middleware('auth');
Route::post('/{locale}/tables/list', '\App\Http\Controllers\TableController@create')->name('tables.add')->middleware('auth');
Route::get('/{locale}/tables/delete/{id}', '\App\Http\Controllers\TableController@destroy')->name('tables.delete')->middleware('auth');

// Overview
Route::get('/{locale}/overview/game', '\App\Http\Controllers\OverviewController@gameOverview')->name('overview.gametable')->middleware('auth');
Route::get('/{locale}/overview/game/public', '\App\Http\Controllers\OverviewController@gameOverviewPublic')->name('overview.gametablepublic');

// Games
Route::get('/{locale}/games/list', '\App\Http\Controllers\GameController@list')->name('games.list')->middleware('auth');
Route::get('/{locale}/games/delete/{id}', '\App\Http\Controllers\GameController@delete')->name('games.delete')->middleware('auth');
Route::get('/{locale}/games/edit/{id}', '\App\Http\Controllers\GameController@edit')->name('games.edit')->middleware('auth');
Route::post('/{locale}/games/edit/post', '\App\Http\Controllers\GameController@edit_post')->name("game.edit.post")->middleware('auth');

// Scoreboard
Route::get('{locale}/scoreboard', "\App\Http\Controllers\ScoreboardController@scoreboardPublic")->name('scoreboard.public');
Route::get('{locale}/scoreboard/guest', "\App\Http\Controllers\ScoreboardController@scoreboardPublicGuest")->name('scoreboard.guest');
Route::get('{locale}/scoreboard/beamer', "\App\Http\Controllers\ScoreboardController@scoreboardScreen")->name('scoreboard.beamer');
Route::get('{locale}/scoreboard/private', "\App\Http\Controllers\ScoreboardController@scoreboardPrivate")->name('scoreboard.private')->middleware(['auth']);

// Account
Route::get('/{locale}/account/', "\App\Http\Controllers\AccountController@view")->name('account.view')->middleware('auth');
Route::get('/{locale}/account/reset/', "\App\Http\Controllers\AccountController@changePasswordPage")->name('account.changePassword');
Route::post('/{locale}/account/reset/','\App\Http\Controllers\AccountController@changePassword')->name('account.changePassword.post');

// Exports
Route::get('exports/gamescores', '\App\Http\Controllers\ExportController@gamescores')->name('export.gamescores')->middleware('auth');
Route::get('exports/remarks', '\App\Http\Controllers\ExportController@remarks')->name('export.remarks')->middleware('auth');

require __DIR__.'/auth.php';

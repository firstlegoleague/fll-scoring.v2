<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Hash;

class setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:all {regionName} {organizerPassword}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setups all the variables for the final';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $regionName = $this->argument("regionName");
        $org_pass = $this->argument("organizerPassword");

        $setting = \App\Models\settings::all()
            ->where('key', 'finalName')
            ->first();
        $setting->value = $regionName;
        $setting->save();

        $org_user = \App\Models\User::all()->where("id", 3)->first();
        $org_user->password = Hash::make($org_pass);
        $org_user->save();

        $this->info("Done with the setup");
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class addTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:tables {amountOfTables} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically generate tables for the final';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(\App\Models\tables::all()->count() != 0 && $this->option("force") == false){
            $this->info("Tables are already inited");
        } else if ($this->option("force") == true){

            $tables = $this->argument("amountOfTables");
            $offset = \App\Models\tables::all()->count();

            for ($i=0; $i < $tables; $i++) { 
                $table = new \App\Models\tables();
                $table->table = "Tafel " . $i+$offset;
                $table->save();
            }
            $this->info("Created {$tables} tables");
        } else {
            $tables = $this->argument("amountOfTables");
            for ($i=0; $i < $tables; $i++) { 
                $table = new \App\Models\tables();
                $table->table = "Tafel " . $i+1;
                $table->save();
            }
            $this->info("Created {$tables} tables");
        }
    }
}

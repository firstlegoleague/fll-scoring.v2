<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class addRounds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:rounds {amountOfRounds} {--force} {--F|finales}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically generate rounds for the final';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (\App\Models\rounds::all()->count() != 0 && $this->option("force") == false) {
            $this->info("Rounds are already inited");
        } else if ($this->option("force") == true) {

            $rounds = $this->argument("amountOfRounds");
            $offset = \App\Models\rounds::all()->count();

            for ($i = 0; $i < $rounds; $i++) {
                $round = new \App\Models\rounds();
                $round->round = "Ronde " . $i + $offset;
                $round->save();
            }
            $this->info("Created {$rounds} rounds");
        } else {
            $rounds = $this->argument("amountOfRounds");
            for ($i = 0; $i < $rounds; $i++) {
                $round = new \App\Models\rounds();
                $round->round = "Ronde " . $i + 1;
                $round->save();
            }
            $this->info("Created {$rounds} rounds");
        }

        if ($this->option("finales")) {
            $round = new \App\Models\rounds();
            $round->round = "Kwart Finale";
            $round->save();

            $round = new \App\Models\rounds();
            $round->round = "Halve Finale";
            $round->save();

            $round = new \App\Models\rounds();
            $round->round = "Finale";
            $round->save();
        }
    }
}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class setupRegion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:name {regionName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Only change the regionname';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $regionName = $this->argument("regionName");

        $setting = \App\Models\settings::all()
            ->where('key', 'finalName')
            ->first();
        $setting->value = $regionName;
        $setting->save();

        $this->info("Done!");
    }
}

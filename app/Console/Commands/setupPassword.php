<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class setupPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:password {organizerPassword}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Just setup the password';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $org_pass = $this->argument("organizerPassword");

        $org_user = \App\Models\User::all()->where("id", 3)->first();
        $org_user->password = Hash::make($org_pass);
        $org_user->save();

        $this->info("Done!");
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class createDemo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:demo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create variables to use this instance as a demo or for development purposes';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $team = new \App\Models\teams();
        $team->teamNumber = 1;
        $team->teamname = "Team 1";
        $team->save();

        $team = new \App\Models\teams();
        $team->teamNumber = 2;
        $team->teamname = "Team 2";
        $team->affiliate = "Fontys";
        $team->save();

        $team = new \App\Models\teams();
        $team->teamNumber = 3;
        $team->teamname = "Team 3";
        $team->affiliate = "JetNet";
        $team->save();

        $team = new \App\Models\teams();
        $team->teamNumber = 4;
        $team->teamname = "Team 4";
        $team->affiliate = "BSO de rare vogels";
        $team->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Ronde 1";
        $ronde->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Ronde 2";
        $ronde->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Ronde 3";
        $ronde->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Kwart finale";
        $ronde->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Halve finale";
        $ronde->save();

        $ronde = new \App\Models\rounds();
        $ronde->round = "Finale";
        $ronde->save();

        $tafel = new \App\Models\tables();
        $tafel->table = "Tafel 1";
        $tafel->save();

        $tafel = new \App\Models\tables();
        $tafel->table = "Tafel 2";
        $tafel->save();

        $tafel = new \App\Models\tables();
        $tafel->table = "Tafel 3";
        $tafel->save();

        $tafel = new \App\Models\tables();
        $tafel->table = "Tafel 4";
        $tafel->save();


    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class createTeam extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:team {teamnumber} {teamname} {--a=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a team to the finale';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $num = $this->argument("teamnumber");
        $name = $this->argument("teamname");

        $team = new \App\Models\teams();
        $team->teamname = $name;
        $team->teamnumber = $num;

        if($this->option("a")){
            $team->affiliate = $this->option("a");
        }
        
        $team->save();
        return 0;
    }
}

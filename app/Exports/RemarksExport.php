<?php

namespace App\Exports;
use App\Teams;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RemarksExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {

        if (\App\Models\settings::getSeason() == 2021) {
            $remarks = \App\Models\challenge2021::all()->whereNotNull('remarks');
        } else if (\App\Models\settings::getSeason() == 2022) {
            $remarks = \App\Models\challenge2022::all()->whereNotNull('remarks');
        }

        return view('exports.remarks', compact("remarks"));
    }
}

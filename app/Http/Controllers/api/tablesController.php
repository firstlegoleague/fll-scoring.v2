<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class tablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response(\App\Models\tables::all()->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($key, Request $request)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $table = new \App\Models\tables();

        $table->table = $request->all()["name"];
        $table->save();

        return response('{"code":200,"response":"New table created with id: '.$table->id.',"id":'.$table->id.'}', 200)->header('Content-Type', 'text/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $table = \App\Models\tables::all()->where("id", $id)->first();

        if($table == null)
            return response('{"code":404,"response":"Table not found"}', 404)->header('Content-Type', 'text/json');

        return response($table->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $id, Request $request)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $table = \App\Models\tables::all()->where('id', $id)->first();

        $table->table = $request->all()["name"];
        $table->save();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $gamesCount = \App\Models\challenge2021::all()->where('tableID', $id)->count();
        if(!($gamesCount == 0))
            return response('{"code":409,"response":"Not allowed to remove a table where games are played"}', 409)->header('Content-Type', 'text/json');

        $table = \App\Models\tables::all()->where('id')->first();
        $table->delete();


        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\teams;
use Illuminate\Http\Request;

class teamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response(\App\Models\teams::all()->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');


        if($request->exists("teamname") == false ||
            $request->exists("teamnumber") == false)
        {
            return response('{"code":417,"response":"Not all parameters has been supplied"}', 417)->header('Content-Type', 'text/json');
        }

        $existingTeam  = \App\Models\teams::all()->where("teamNumber", $request->all()['teamnumber'])->first();
        if($existingTeam != NULL){
            return response('{"code":409,"response":"Teamnumber already exsists"}', 409)->header('Content-Type', 'text/json');
        }

        $team = new \App\Models\teams;
        $team->teamname = $request->all()['teamname'];
        $team->teamNumber = $request->all()['teamnumber'];

        if($request->exists("affiliate"))
            $team->affiliate = $request->all()['affiliate'];

        $team->save();

        return response('{"code":200,"response":"Success","info":"New teamid: '.$team->id.'"}', 200)->header('Content-Type', 'text/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\teams  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $team = \App\Models\teams::all()->where('id', $id)->first();

        if($team == null)
            return response('{"code":404,"response":"Team not found"}', 404)->header('Content-Type', 'text/json');

        return response($team->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $team = \App\Models\teams::all()->where('id', $id)->first();

        if($request->exists('teamnumber'))
            $team['teamNumber'] = $request->all()['teamnumber'];

        if($request->exists('teamname'))
            $team['teamname'] = $request->all()['teamname'];

        if($request->exists('affiliate'))
            $team['affiliate'] = $request->all()['affiliate'];

        $team->save();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\teams  $teams
     * @return \Illuminate\Http\Response
     */
    public function delete($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        // Get the team
        $team = \App\Models\teams::all()->where('id', $id)->first();

        // Check if the team has results
        if(\App\Models\teams::hasResults($id))
            return response('{"code":409,"response":"Not allowed to remove team who already has played games"}', 409)->header('Content-Type', 'text/json');

        // Remove
        $team->delete();
        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
}

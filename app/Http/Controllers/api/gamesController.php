<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class gamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $games = \App\Models\challenge2021::all();
        return response($games->toJson())->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $games = \App\Models\challenge2021::all()->where('id', $id);
        return response($games->toJson());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response('{"code":501,"response":"Not implemented"}', 501)->header('Content-Type', 'text/json');
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\rounds;
use Illuminate\Http\Request;

class roundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($key)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        return response(\App\Models\rounds::all()->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($key, Request $request)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');


        if($request->exists("name") == false)
            return response('{"code":417,"response":"Not all parameters has been supplied"}', 417)->header('Content-Type', 'text/json');


        $round = new \App\Models\rounds();
        $round->round = $request->all()["name"];
        $round->save();

        return response('{"code":200,"response":"New round created with id: '.$round->id.',"id":'.$round->id.'}', 200)->header('Content-Type', 'text/json');

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function show($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $round = \App\Models\rounds::all()->where('id', $id)->first();

        if($round == null)
            return response('{"code":404,"response":"Round not found"}', 404)->header('Content-Type', 'text/json');

        return response($round->toJson(), 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function edit($key, $id, Request $request)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $round = \App\Models\rounds::all()->where('id', $id)->first();

        $round->round = $request->all()["name"];
        $round->save();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function delete($key, $id)
    {
        if(!\App\Models\api::checkKey($key))
            return response('{"code":401,"response":"Not allowed"}', 401)->header('Content-Type', 'text/json');

        $gamesCount = \App\Models\challenge2021::all()->where('roundID', $id)->count();
        if(!($gamesCount == 0))
            return response('{"code":409,"response":"Not allowed to remove a round where games are played"}', 409)->header('Content-Type', 'text/json');

        $round = \App\Models\rounds::all()->where('id')->first();
        $round->delete();

        return response('{"code":200,"response":"Success"}', 200)->header('Content-Type', 'text/json');
    }
}

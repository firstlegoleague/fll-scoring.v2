<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Challenge2022Controller extends Controller
{
    function saveScoresheet(Request $request, $locale){
        App::setLocale($locale);

        $info = $request->all();

        if($info["game_id"] == null){
            $newScore = new \App\Models\challenge2022();
        } else {
            $newScore = \App\Models\challenge2022::all()->where("id", $info["game_id"])->first();
        }


        $newScore->M00_1 = $info["M00_1"];
        $newScore->M01_1 = $info["M01_1"];
        $newScore->M02_1 = $info["M02_1"];
        $newScore->M02_2 = $info["M02_2"];
        $newScore->M03_1 = $info["M03_1"];
        $newScore->M03_2 = $info["M03_2"];
        $newScore->M04_1 = $info["M04_1"];
        $newScore->M05_1 = $info["M05_1"];
        $newScore->M05_2 = $info["M05_2"];
        $newScore->M06_1 = $info["M06_1"];
        $newScore->M06_2 = $info["M06_2"];
        $newScore->M07_1 = $info["M07_1"];
        $newScore->M08_1 = $info["M08_1"];
        $newScore->M08_2 = $info["M08_2"];
        $newScore->M09_1 = $info["M09_1"];
        $newScore->M09_2 = $info["M09_2"];
        $newScore->M10_1 = $info["M10_1"];
        $newScore->M11_1 = $info["M11_1"];
        $newScore->M12_1 = $info["M12_1"];
        $newScore->M12_2 = $info["M12_2"];
        $newScore->M13_1 = $info["M13_1"];
        $newScore->M14_1 = $info["M14_1"];
        $newScore->M14_2 = $info["M14_2"];
        $newScore->M15_1 = $info["M15_1"];
        $newScore->precision = $info["M17_1"];
        $newScore->GP_1 = $info["GP_1"];

        // General information

        $teamfull = $info["team"];
        $teamnumber = explode(" ", $teamfull)[0];
        $team = \App\Models\teams::all()->where("teamNumber", $teamnumber)->first();
        $round = \App\Models\rounds::all()->where("id", $info["round"])->first();
        $table = \App\Models\tables::all()->where("id", $info["table"])->first();

        $newScore->teamID = $team->id;
        $newScore->roundID = $round->id;
        $newScore->tableID = $table->id;
        $newScore->judge_id = Auth::user()->id;
        $newScore->sendedScore = $info["totalScore"];
        $newScore->remarks = $info["remarks"];
        $newScore->isPublic =  \App\Models\rounds::all()->where('id', $round->id)->first()->public; // \App\settings::getGamePublic();

        // Saves the last table, so it automatically sets the table next round
        $user = Auth::user();
        $user->lastTable = $table->id;
        $user->lastRound = $round->id;
        $user->save();

        $score = 0;

        // M00
        if($newScore->M00_1 == 1){
            $score += 20;
        }

        // M01
        if($newScore->M01_1 == 1){
            $score += 10;
        }

        // M02
        $score += $newScore->M02_1*5;

        if($newScore->M02_1 != 0 && $newScore->M02_2 == 1){
            $score += 10;
        }

        //M03
        $score += $newScore->M03_1 * 10;

        if($newScore->M03_2 == 1){
            $score += 5;
        }

        //M04
        $score += $newScore->M04_1 * 5;

        if($newScore->M04_1 == 3){
            $score += 5;
        }

        // M05
        if($newScore->M05_1 == 1){
            $score += 20;
        }

        if($newScore->M05_2 == 1){
            $score += 10;
        }

        // M06
        if($newScore->M06_1 == 1){
            $score += 10;
        }

        if($newScore->M06_2 == 1){
            $score += 10;
        }

        //M07
        $score += $newScore->M07_1 * 10;

        //M08
        if($newScore->M08_1 == 1){
            $score += 10;
        }
        if($newScore->M08_2 == 1){
            $score += 10;
        }

        //M09
        if($newScore->M09_1 == 1){
            $score += 10;
        }

        if($newScore->M09_2 == 1){
            $score += 10;
        } else if($newScore->M09_2 == 2){
            $score += 20;
        }

        //M10
        $score += $newScore->M10_1 * 5;
        if($newScore->M10_1 == 3){
            $score += 10;
        }

        //M11
        if($newScore->M11_1 == 1){
            $score += 20;
        }

        //M12
        $score += $newScore->M12_1 * 5;
        $score += $newScore->M12_2 * 10;

        //M13
        $score += $newScore->M13_1 * 5;

        //M14
        $score += $newScore->M14_1 * 5;
        $score += $newScore->M14_2 * 10;

        //M15
        $score += $newScore->M15_1 * 5;

        if($newScore->precision == 1){
            $score += 10;
        } else if($newScore->precision == 2){
            $score += 15;
        } else if($newScore->precision == 3) {
            $score += 25;
        } else if($newScore->precision == 4){
            $score += 35;
        } else if($newScore->precision == 5){
            $score += 50;
        } else if($newScore->precision == 6){
            $score += 50;
        }

        $newScore->totalScore = $score;
        $newScore->save();

        return Redirect()->back()->with("successSaved", "true");
    }
}

<?php

namespace App\Http\Controllers;

use App\Exports\ChallengeExport2021;
use App\Exports\RemarksExport;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Excel;

class ExportController extends Controller
{
    //
    public function gamescores(){

        $exportName = "GameExport.xlsx";
//        $exportName = $exportName + "".Carbon::now()->timestamp."";
//        $exportName = $exportName + ".xlxs";

        return Excel::download(new ChallengeExport2021(), $exportName);
    }

    public function remarks(){
        $exportName = "RemarksExport.xlsx";
        //return Excel::download(new RemarksExport(), $exportName);
        return Excel::download(new RemarksExport(), $exportName);
    }
}

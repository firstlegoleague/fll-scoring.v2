<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Database\Eloquent\SoftDeletes;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        App::setLocale($locale);
        $tables = \App\Models\tables::all();

        return view("tables.list", compact("tables"));
    }

    public function add_page($locale){
        \Illuminate\Support\Facades\App::setLocale($locale);
        return view("tables.add");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $locale)
    {
        App::setLocale($locale);

        $newTable = new \App\Models\tables();
        $info = $request->request->all();

        $newTable->table = $info["table"];
        $newTable->save();


        $tables = \App\Models\tables::all();
        return view("tables.list", compact("tables"));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        //$table = \App\Models\tables::all(); // ->where('id', $id)->first();

        \App\Models\tables::destroy($id);

        return redirect()->route('tables.list', ['locale'=>$locale]);
    }
}

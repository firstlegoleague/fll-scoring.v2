<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ScoreboardController extends Controller
{
    public function scoreboardPublic($locale){
        App::setLocale($locale);
        $teams = \App\Models\teams::all();

        $rankings = [];

        foreach ($teams as $team){
            $year = \App\Models\settings::getSeason();

            if($year == 2021){
                $bestGame = \App\Models\challenge2021::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2021::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore");
            } else if($year == 2022){
                $bestGame = \App\Models\challenge2022::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2022::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore");
            }
            else {
                return false;
            }

            $weight = 1;
            $weightScore = 0;
            foreach ($allGames as $game){
                $weightScore += ($game->totalScore * $weight);
                $weight = $weight/1000;
            }

            if($bestGame != null) {
                $bestGame['weight'] = $weightScore;
                array_push($rankings, $bestGame);
            }
        }


        $games = collect($rankings)->sortByDesc("weight");
//        dd($games);
        return view('scoreboard.rankings', compact("games"));
    }

    public function scoreboardPublicGuest($locale){
        App::setLocale($locale);
        $teams = \App\Models\teams::all();

        $rankings = [];

        foreach ($teams as $team){
            $year = \App\Models\settings::getSeason();

            if($year == 2021){
                $bestGame = \App\Models\challenge2021::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2021::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore");
            } else if($year == 2022){
                $bestGame = \App\Models\challenge2022::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2022::all()->where('teamID', $team->id)->where('isPublic', 1)->sortByDesc("totalScore");
            }
            else {
                return false;
            }

            $weight = 1;
            $weightScore = 0;
            foreach ($allGames as $game){
                $weightScore += ($game->totalScore * $weight);
                $weight = $weight/1000;
            }

            if($bestGame != null) {
                $bestGame['weight'] = $weightScore;
                array_push($rankings, $bestGame);
            }
        }


        $games = collect($rankings)->sortByDesc("weight");
//        dd($games);
        return view('scoreboard.rankings-public', compact("games"));
    }

    public function scoreboardPrivate($locale){
        App::setLocale($locale);
        $teams = \App\Models\teams::all();

        $rankings = [];

        foreach ($teams as $team){
            $year = \App\Models\settings::getSeason();

            if($year == 2021){
                $bestGame = \App\Models\challenge2021::all()->where('teamID', $team->id)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2021::all()->where('teamID', $team->id)->sortByDesc("totalScore");
            } else if($year == 2022){
                $bestGame = \App\Models\challenge2022::all()->where('teamID', $team->id)->sortByDesc("totalScore")->first();
                $allGames = \App\Models\challenge2022::all()->where('teamID', $team->id)->sortByDesc("totalScore");
            }
            else {
                return false;
            }

            $weight = 1;
            $weightScore = 0;
            foreach ($allGames as $game){
                $weightScore += ($game->totalScore * $weight);
                $weight = $weight/1000;
            }

            if($bestGame != null) {
                $bestGame['weight'] = $weightScore;
                array_push($rankings, $bestGame);
            }
        }

        $rankings = collect($rankings)->sortByDesc("weight");
        return view('scoreboard.rankings', ["games"=>$rankings->all()]);
    }

    public function scoreboardScreen($locale){
        App::setLocale($locale);
        $teams = \App\Models\teams::all();

        $rankings = [];

        foreach ($teams as $team){
            $year = \App\Models\settings::getSeason();

            if($year == 2021){
                $gamesTeam = \App\Models\challenge2021::all()->where('teamID', $team->id)->where('isPublic',1)->sortByDesc("totalScore")->first();
            } else if($year == 2022){
                $gamesTeam = \App\Models\challenge2022::all()->where('teamID', $team->id)->where('isPublic',1)->sortByDesc("totalScore")->first();
            }
            else {
                return false;
            }

            if($gamesTeam != null) {
                array_push($rankings, $gamesTeam);
            }
        }

        $rankings = collect($rankings)->sortByDesc("totalScore");

        return view('scoreboard.database', ["games"=>$rankings]);
    }
}

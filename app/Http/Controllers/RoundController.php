<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RoundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list($locale){
        App::setLocale($locale);
        $rounds = \App\Models\rounds::all();

        return view("rounds.list", compact("rounds"));
    }

    public function add_page($locale){
        App::setLocale($locale);
        return view("rounds.add");
    }

    public function add(Request $request, $locale){
        App::setLocale($locale);

        $newRound = new \App\Models\rounds();
        $info = $request->request->all();

        $newRound->round = $info["round"];
        $newRound->save();


        $rounds = \App\Models\rounds::all();
        return view("rounds.list", compact("rounds"));
    }

    public function delete($locale, $id){
        $round = \App\Models\rounds::all()->where('id', $id)->first();
        $round->delete();

        return redirect()->route('rounds.list', ['locale'=>$locale]);
    }

    public function setPublic($id){
        $round = \App\Models\rounds::all()->where('id', $id)->first();
        $round->public = 1;
        $round->save();

        if(\App\Models\settings::getSeason() == "2021"){
            \App\Models\challenge2021::where('roundID', $id)->update(['isPublic'=>1]);
        } else if(\App\Models\settings::getSeason() == "2022"){
            \App\Models\challenge2022::where('roundID', $id)->update(['isPublic'=>1]);
        }
    }


    public function setPrivate($id){
        $round = \App\Models\rounds::all()->where('id', $id)->first();
        $round->public = 0;
        $round->save();

        if(\App\Models\settings::getSeason() == "2021"){
            \App\Models\challenge2021::where('roundID', $id)->update(['isPublic'=>0]);
        } else if(\App\Models\settings::getSeason() == "2022"){
            \App\Models\challenge2022::where('roundID', $id)->update(['isPublic'=>0]);
        }

    }
}

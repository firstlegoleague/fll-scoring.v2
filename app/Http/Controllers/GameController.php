<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;

class GameController extends Controller
{
    public function list($locale){
        App::setLocale($locale);

        if(\App\Models\settings::getSeason() == 2021){
            $games = \App\Models\challenge2021::all();
        }
        if(\App\Models\settings::getSeason() == 2022){
            $games = \App\Models\challenge2022::all();
        }


        return view("games.list", compact("games"), ["locale"=>$locale]);
    }

    public function delete($locale, $id){
        App::setLocale($locale);

        if(\App\Models\settings::getSeason() == 2021){
            $delGame = \App\Models\challenge2021::all()->where("id", $id)->first();
            $delGame->delete();

            $games = \App\Models\challenge2021::all();
        }


        if(\App\Models\settings::getSeason() == 2022){
            $delGame = \App\Models\challenge2022::all()->where("id", $id)->first();
            $delGame->delete();

            $games = \App\Models\challenge2022::all();
        }


        return view("games.list", compact("games"), ["locale"=>$locale]);
    }

    public function edit($locale, $id){
        App::setLocale($locale);

        $rounds = \App\Models\rounds::all();

        if(\App\Models\settings::getSeason() == 2021){
            $game = \App\Models\challenge2021::all()->where("id", $id)->first();
        } else if(\App\Models\settings::getSeason() == 2022){
            $game = \App\Models\challenge2022::all()->where("id", $id)->first();
        }

        $teams = \App\Models\teams::all();
        $rounds = \App\Models\rounds::all();
        $tables = \App\Models\tables::all();


        return view("challenges.".\App\Models\settings::getSeason(), compact("game", "rounds", "teams", "rounds", "tables"), ["locale"=>$locale]);
    }
}

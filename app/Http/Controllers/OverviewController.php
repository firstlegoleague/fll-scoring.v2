<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class OverviewController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }


    public function gameOverview($locale){
        App::setLocale($locale);
        $rounds = \App\Models\rounds::all();
        $teams = \App\Models\teams::all();

        return view("overview.game", compact("rounds","teams"));
    }

    public function gameOverviewPublic($locale){
        App::setLocale($locale);
        $rounds = \App\Models\rounds::all();
        $teams = \App\Models\teams::all();

        return view("overview.gamePublic", compact("rounds","teams"));
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class challenge2021 extends Model
{
    protected $table = "challenge2021";
    use HasFactory;
}

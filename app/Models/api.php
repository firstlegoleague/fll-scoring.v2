<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class api extends Model
{
    use HasFactory;

    protected $table = 'api';

    public static function checkKey($key){

        $validKey = \App\Models\api::all()->where('apikey', $key)->first();

        if($validKey == NULL or $validKey->enabled == 0)
            return false;

        return true;
    }

}

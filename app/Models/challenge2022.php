<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class challenge2022 extends Model
{
    protected $table = "challenge2022";
    use HasFactory;
}

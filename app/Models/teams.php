<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class teams extends Model
{
    use HasFactory;

    static public function hasResults($id){
        $year = \App\Models\settings::getSeason();
        if($year == 2021){
            $amount = \App\Models\challenge2021::all()
                ->where('teamID', $id)
                ->count();
        } else if($year == 2022){
            $amount = \App\Models\challenge2022::all()
                ->where('teamID', $id)
                ->count();
        }
        else {
            return 1;
        }

        if($amount == 0){
            return 0;
        } else {
            return 1;
        }
    }
}

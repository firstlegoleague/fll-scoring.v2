<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class settings extends Model
{
    use HasFactory;
    static public function getSeason(){
        return \App\Models\settings::all()
            ->where('key', 'season')
            ->first()->value;
    }

    static public function getFinalName(){
        return \App\Models\settings::all()
            ->where('key', 'finalName')
            ->first()->value;
    }

    static public function getRegister(){
        return \App\Models\settings::all()
            ->where('key', 'register')
            ->first()->value;
    }

    static public function getGamePublic(){
        return \App\Models\settings::all()
            ->where('key', 'gamePublic')
            ->first()->value;
    }
}
